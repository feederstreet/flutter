import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class FriendRequestsResponse {
  GenericResponse genericResponse;
  List<User> friend_requests = new List();

  FriendRequestsResponse({this.genericResponse, this.friend_requests});

  FriendRequestsResponse.fromJson(Map<String, dynamic> json) {
    genericResponse = GenericResponse.fromJson(json);
    if (json['friend_requests'] != null) {
      if (friend_requests != null) friend_requests.clear();
      friend_requests = new List<User>();
      json['friend_requests'].forEach((v) {
        friend_requests.add(new User.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genericResponse != null) {
      data['success'] = this.genericResponse.success;
      if (this.genericResponse.error != null) {
        data['error'] = this.genericResponse.error.toJson();
      }
    }
    if (this.friend_requests != null) {
      data['friend_requests'] =
          this.friend_requests.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'FriendsResponse{genericResponse: $genericResponse, friend_requests: $friend_requests}';
  }
}
