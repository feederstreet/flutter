import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class LobbyResponse {
  GenericResponse _genericResponse;
  Lobby _lobby;

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) {
    _genericResponse = value;
  }

  Lobby get lobby => _lobby;

  set lobby(Lobby lobby) => _lobby = lobby;

  LobbyResponse({GenericResponse genericResponse, Lobby lobby}) {
    this._genericResponse = genericResponse;
    this._lobby = lobby;
  }

  LobbyResponse.fromJson(Map<String, dynamic> json) {
    _genericResponse = GenericResponse.fromJson(json);
    if (_genericResponse.success) {
      _lobby = json['lobby'] != null ? new Lobby.fromJson(json['lobby']) : null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._genericResponse != null) {
      data['success'] = this._genericResponse.success;
      if (this._genericResponse.error != null) {
        data['error'] = this._genericResponse.error.toJson();
      }
      if (this._lobby != null) {
        data['lobby'] = this._lobby.toJson();
      }
    }
    return data;
  }

  @override
  String toString() {
    return 'LobiesResponse{_genericResponse: $_genericResponse, _lobby: $_lobby}';
  }
}
