import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class LobbiesResponse {
  GenericResponse _genericResponse;
  List<Lobby> _lobbies = new List();

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) {
    _genericResponse = value;
  }

  List<Lobby> get lobbies => _lobbies;

  set lobbies(List<Lobby> value) {
    _lobbies = value;
  }

  LobbiesResponse({GenericResponse genericResponse, List<Lobby> lobbies}) {
    this._genericResponse = genericResponse;
    this._lobbies = lobbies;
  }

  LobbiesResponse.fromJson(Map<String, dynamic> json) {
    _genericResponse = GenericResponse.fromJson(json);
    if (_genericResponse.success) {
      if (json['lobbies'] != null) {
        _lobbies.clear();
        json['lobbies'].forEach((v) {
          _lobbies.add(new Lobby.fromJson(v));
        });
      } else {
        _lobbies = new List();
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._genericResponse != null) {
      data['success'] = this._genericResponse.success;
      if (this._genericResponse.error != null) {
        data['error'] = this._genericResponse.error.toJson();
      }
      if (this._lobbies != null) {
        data['lobbies'] = this._lobbies.map((v) => v.toJson()).toList();
      }
    }
    return data;
  }

  @override
  String toString() {
    return 'LobiesResponse{_genericResponse: $_genericResponse, _lobbies: $_lobbies}';
  }
}
