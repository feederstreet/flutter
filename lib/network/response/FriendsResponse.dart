import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class FriendsResponse {
  GenericResponse genericResponse;
  List<User> friends = new List();

  FriendsResponse({this.genericResponse, this.friends});

  FriendsResponse.fromJson(Map<String, dynamic> json) {
    genericResponse = GenericResponse.fromJson(json);
    if (json['friends'] != null) {
      if (friends != null) friends.clear();
      friends = new List<User>();
      json['friends'].forEach((v) {
        friends.add(new User.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genericResponse != null) {
      data['success'] = this.genericResponse.success;
      if (this.genericResponse.error != null) {
        data['error'] = this.genericResponse.error.toJson();
      }
    }
    if (this.friends != null) {
      data['friends'] = this.friends.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'FriendsResponse{genericResponse: $genericResponse, friends: $friends}';
  }
}
