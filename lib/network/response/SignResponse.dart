import 'package:gamerhub/network/response/GenericResponse.dart';

class SignResponse {
  String _tokenKey;
  GenericResponse _genericResponse;

  SignResponse({GenericResponse genericResponse, String tokenKey}) {
    this._genericResponse = genericResponse;
    this._tokenKey = tokenKey;
  }

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) {
    _genericResponse = value;
  }

  String get tokenKey => _tokenKey;

  set tokenKey(String tokenKey) => _tokenKey = tokenKey;

  SignResponse.fromJson(Map<String, dynamic> json) {
    _genericResponse = GenericResponse.fromJson(json);
    if (_genericResponse.success) {
      _tokenKey = json['token_key'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._genericResponse != null) {
      data['success'] = this._genericResponse.success;
      if (this._genericResponse.error != null) {
        data['error'] = this._genericResponse.error.toJson();
      }
      data['token_key'] = this._tokenKey;
    }
    return data;
  }

  @override
  String toString() {
    return 'SignResponse{_tokenKey: $_tokenKey, _genericResponse: $_genericResponse}';
  }
}
