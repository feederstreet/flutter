import 'package:gamerhub/model/Conversation.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class ConversationResponse {
  GenericResponse _genericResponse;
  List<Conversation> _inbox = new List();

  ConversationResponse(
      {GenericResponse genericResponse, List<Conversation> inbox}) {
    this._genericResponse = genericResponse;
    this._inbox = inbox;
  }

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) => _genericResponse = value;

  List<Conversation> get inbox => _inbox;

  set inbox(List<Conversation> value) => _inbox = value;

  ConversationResponse.fromJson(Map<String, dynamic> json) {
    genericResponse = GenericResponse.fromJson(json);

    if (json.containsKey('inbox')) {
      inbox = new List<Conversation>();
      json['inbox'].forEach((v) {
        inbox.add(new Conversation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genericResponse != null) {
      data['success'] = this.genericResponse.success;
      if (this.genericResponse.error != null) {
        data['error'] = this.genericResponse.error.toJson();
      }
    }

    if (this._inbox != null) {
      data['inbox'] = this._inbox.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'ConversationResponse{_genericResponse: $_genericResponse, _inbox: $_inbox}';
  }
}
