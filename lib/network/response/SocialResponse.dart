class SocialResponse {
  String _name;
  String _email;
  String _id;

  SocialResponse({String name, String email, String id}) {
    this._name = name;
    this._email = email;
    this._id = id;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get id => _id;
  set id(String id) => _id = id;

  SocialResponse.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _email = json['email'];
    _id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['email'] = this._email;
    data['id'] = this._id;
    return data;
  }

  @override
  String toString() {
    return 'SocialResponse{_name: $_name, _email: $_email, _id: $_id}';
  }
}
