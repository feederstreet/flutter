import 'dart:io';

import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class ErrorResponse {
  int _code;
  String _text;

  ErrorResponse({int code}) {
    this._code = code;
    if (this._code != null) {
      _text = Constants.getCodeString(_code);
    }
  }

  int get code => _code;

  set code(int code) => _code = code;

  String get text => _text;

  set text(String value) {
    _text = value;
  }

  ErrorResponse.fromJson(Map<String, dynamic> json) {
    print("error json =>" + json.toString());
    if (json.containsKey('code')) {
      try {
        _code = json['code'];
      } catch (e) {
        _code = int.tryParse(json['code'].toString());
      } finally {
        if (code == 4001) {
          SharedPreferencesHelper.clearSharedPreference();
          exit(0);
        } else {
          _text = Constants.getCodeString(_code);
        }
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.code != null) data['code'] = this._code;
    return data;
  }

  @override
  String toString() {
    return 'ErrorResponse{_code: $_code}';
  }
}
