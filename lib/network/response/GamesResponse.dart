import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class GamesResponse {
  GenericResponse _genericResponse;
  List<Game> _games = new List();

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) {
    _genericResponse = value;
  }

  List<Game> get games => _games;

  set games(List<Game> value) {
    _games = value;
  }

  GamesResponse.fromJson(Map<String, dynamic> json) {
    _genericResponse = GenericResponse.fromJson(json);
    if (_genericResponse.success) {
      if (json['games'] != null) {
        _games.clear();
        json['games'].forEach((v) {
          _games.add(new Game.fromJson(v));
        });
      } else {
        _games = new List();
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._genericResponse != null) {
      data['success'] = this._genericResponse.success;
      if (this._genericResponse.error != null) {
        data['error'] = this._genericResponse.error.toJson();
      }
      if (this._games != null) {
        data['games'] = this._games.map((v) => v.toJson()).toList();
      }
    }
    return data;
  }

  @override
  String toString() {
    return 'GamesResponse{_genericResponse: $_genericResponse, _games: $_games}';
  }
}
