import 'package:gamerhub/network/response/ErrorResponse.dart';

class GenericResponse {
  bool _success;
  ErrorResponse _error;

  GenericResponse({bool success, ErrorResponse error, String tokenKey}) {
    this._success = success;
    this._error = error;
  }

  bool get success => _success;

  set success(bool success) => _success = success;

  ErrorResponse get error => _error;

  set error(ErrorResponse error) => _error = error;

  GenericResponse.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    if (!_success) {
      if (json['error'] != null)
        _error = json['error'] = new ErrorResponse.fromJson(json['error']);
      else {
        _error = json['error'] = new ErrorResponse(code: -1);
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    if (this._error != null) {
      data['error'] = this._error.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return 'GenericResponse{_success: $_success, _error: $_error}';
  }
}
