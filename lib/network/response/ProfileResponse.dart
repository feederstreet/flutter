import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class ProfileResponse {
  GenericResponse genericResponse;
  User user;
  bool isSameUser;

  ProfileResponse({this.genericResponse, this.user, this.isSameUser});

  ProfileResponse.fromJson(Map<String, dynamic> json) {
    genericResponse = GenericResponse.fromJson(json);
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    isSameUser = json['is_same_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genericResponse != null) {
      data['success'] = this.genericResponse.success;
      if (this.genericResponse.error != null) {
        data['error'] = this.genericResponse.error.toJson();
      }
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['is_same_user'] = this.isSameUser;
    return data;
  }
}
