import 'package:gamerhub/model/Message.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';

class InboxResponse {
  GenericResponse _genericResponse;
  List<Message> _inbox = new List();

  InboxResponse({GenericResponse genericResponse, List<Message> inbox}) {
    this._genericResponse = genericResponse;
    this._inbox = inbox;
  }

  GenericResponse get genericResponse => _genericResponse;

  set genericResponse(GenericResponse value) => _genericResponse = value;

  List<Message> get inbox => _inbox;

  set inbox(List<Message> value) => _inbox = value;

  InboxResponse.fromJson(Map<String, dynamic> json) {
    genericResponse = GenericResponse.fromJson(json);

    if (json.containsKey('inbox')) {
      inbox = new List<Message>();
      json['inbox'].forEach((v) {
        inbox.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genericResponse != null) {
      data['success'] = this.genericResponse.success;
      if (this.genericResponse.error != null) {
        data['error'] = this.genericResponse.error.toJson();
      }
    }

    if (this._inbox != null) {
      data['inbox'] = this._inbox.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'InboxResponse{_genericResponse: $_genericResponse, _inbox: $_inbox}';
  }
}
