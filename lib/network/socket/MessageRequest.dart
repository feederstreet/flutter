class MessageRequest {
  String _type;
  String _to;
  String _content;
  String _tokenKey;

  MessageRequest({String type, String to, String content, String tokenKey}) {
    this._type = type;
    this._to = to;
    this._content = content;
    this._tokenKey = tokenKey;
  }

  String get type => _type;
  set type(String type) => _type = type;
  String get to => _to;
  set to(String to) => _to = to;
  String get content => _content;
  set content(String content) => _content = content;
  String get tokenKey => _tokenKey;
  set tokenKey(String tokenKey) => _tokenKey = tokenKey;

  MessageRequest.fromJson(Map<String, dynamic> json) {
    _type = json['type'];
    _to = json['to'];
    _content = json['content'];
    _tokenKey = json['token_key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this._type;
    data['to'] = this._to;
    data['content'] = this._content;
    data['token_key'] = this._tokenKey;
    return data;
  }

  @override
  String toString() {
    return 'Message{_type: $_type, _to: $_to, _content: $_content, _tokenKey: $_tokenKey}';
  }
}
