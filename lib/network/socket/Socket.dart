import 'dart:async';

class SocketHelper {
  List<SocketListener> listenerList;

  static final SocketHelper _instance = new SocketHelper._internal();

  factory SocketHelper() {
    return _instance;
  }

  SocketHelper._internal() {
    if (this.listenerList == null) {
      this.listenerList = new List();
    }
    _create();
  }

  bool addListener(SocketListener listener) {
    if (this.listenerList != null && listener != null) {
      if (!this.listenerList.contains(listener)) {
        this.listenerList.add(listener);
        return true;
      }
    }
    return false;
  }

  bool removeListener(SocketListener listener) {
    if (this.listenerList != null && listener != null) {
      if (this.listenerList.contains(listener)) {
        this.listenerList.remove(listener);
        return true;
      }
    }
    return false;
  }

  Future _create() async {
    try {} catch (e) {
      print("WebSocket Cache Error => " + e.toString());
    }
  }

  Future<bool> sendToPerson(String targetId, String text) async {
    /* if (socketIO != null) {
      MessageRequest message = new MessageRequest(
        type: Constants.SOCKET_MSG_TYPE_USER,
        to: targetId,
        content: text,
        tokenKey: await SharedPreferencesHelper.getToken(),
      );
      //socketIO..sink.add(message.toJson());
      return true;
    }*/
    return false;
  }

  Future<bool> connect() async {
    return true;
  }

  Future<bool> disconnect() async {
    return false;
  }

  _socketStatus(dynamic data) {
    print("Socket status: " + data);
  }
}

class SocketListener {
  void receiveMessage() async {}

  void onDone() async {}

  void onError() async {}
}
