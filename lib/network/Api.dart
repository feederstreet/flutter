import 'dart:async';
import 'dart:convert';

import 'package:gamerhub/network/response/ConversationResponse.dart';
import 'package:gamerhub/network/response/FriendRequestsResponse.dart';
import 'package:gamerhub/network/response/FriendsResponse.dart';
import 'package:gamerhub/network/response/GamesResponse.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/network/response/InboxResponse.dart';
import 'package:gamerhub/network/response/LobbiesResponse.dart';
import 'package:gamerhub/network/response/LobbyResponse.dart';
import 'package:gamerhub/network/response/ProfileResponse.dart';
import 'package:gamerhub/network/response/SignResponse.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:http/http.dart' as http;

class Api {
  static Future<SignResponse> signUp(Map body) async {
    print("signUp " + body.toString());
    return http.post(
      Constants.API_BASE_URL + "/auth/register",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      SignResponse signResponse =
          SignResponse.fromJson(json.decode(response.body));

      if (signResponse.genericResponse.success) {
        await SharedPreferencesHelper.putString(
            signResponse.tokenKey, Constants.SP_TOKEN_KEY);
      }

      return signResponse;
    });
  }

  static Future<SignResponse> signIn(Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/auth/login",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      SignResponse signResponse =
          SignResponse.fromJson(json.decode(response.body));

      if (signResponse.genericResponse.success) {
        await SharedPreferencesHelper.putString(
            signResponse.tokenKey, Constants.SP_TOKEN_KEY);
      }

      return signResponse;
    });
  }

  static Future<SignResponse> signInWithFacebook(Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/auth/login_with_facebook",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      SignResponse signResponse =
          SignResponse.fromJson(json.decode(response.body));

      if (signResponse.genericResponse.success) {
        await SharedPreferencesHelper.putString(
            signResponse.tokenKey, Constants.SP_TOKEN_KEY);
      }

      return signResponse;
    });
  }

  static Future<SignResponse> signInWithGoogle(Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/auth/login_with_google",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      SignResponse signResponse =
          SignResponse.fromJson(json.decode(response.body));

      if (signResponse.genericResponse.success) {
        await SharedPreferencesHelper.putString(
            signResponse.tokenKey, Constants.SP_TOKEN_KEY);
      }

      return signResponse;
    });
  }

  static Future<GenericResponse> changeSettings(String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/edit_settings",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      return GenericResponse.fromJson(json.decode(response.body));
    });
  }

  static Future<ProfileResponse> getProfile(String token) async {
    return http.get(
      Constants.API_BASE_URL + "/user/get_profile",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      ProfileResponse profileResponse =
          ProfileResponse.fromJson(json.decode(response.body));

      if (profileResponse.genericResponse.success) {
        await SharedPreferencesHelper.putJson(
            json.decode(response.body)['user'], Constants.SP_USER_KEY);
      }
      return profileResponse;
    });
  }

  static Future<ProfileResponse> getFriendProfile(
      String token, String targetId) async {
    return http.get(
      Constants.API_BASE_URL + "/user/get_profile/" + targetId,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      ProfileResponse profileResponse =
          ProfileResponse.fromJson(json.decode(response.body));

      return profileResponse;
    });
  }

  static Future<GenericResponse> isFriend(String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/user/is_friend",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      return genericResponse;
    });
  }

  static Future<FriendsResponse> getFriends(String token) async {
    return http.post(
      Constants.API_BASE_URL + "/user/get_friends",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      FriendsResponse friendsResponse =
          FriendsResponse.fromJson(json.decode(response.body));

      return friendsResponse;
    });
  }

  static Future<FriendRequestsResponse> getFriendRequests(String token) async {
    return http.post(
      Constants.API_BASE_URL + "/user/get_friend_requests",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      FriendRequestsResponse friendRequestsResponse =
          FriendRequestsResponse.fromJson(json.decode(response.body));

      return friendRequestsResponse;
    });
  }

  static Future<GenericResponse> ignoreFriendRequest(
      String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/ignore_friend_request",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<GenericResponse> acceptFriendRequest(
      String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/accept_friend_request",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<GenericResponse> addFriend(String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/add_friend",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<GenericResponse> removeFriend(String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/remove_friend",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<GenericResponse> cancelFriendRequest(
      String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/cancel_friend_request",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<GenericResponse> hasFriendRequest(
      String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/user/is_friend_request_sent",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }

      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) await getProfile(token);

      return genericResponse;
    });
  }

  static Future<LobbiesResponse> getLobbies(String token, Map body) async {
    return http.post(
      Constants.API_BASE_URL + "/lobby/get_lobbies",
      body: json.encode(body),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      LobbiesResponse lobisResponse =
          LobbiesResponse.fromJson(json.decode(response.body));

      if (lobisResponse.genericResponse.success) {}

      return lobisResponse;
    });
  }

  static Future<LobbyResponse> getLobby(String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/lobby/get_lobby",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      LobbyResponse lobbyResponse =
          LobbyResponse.fromJson(json.decode(response.body));

      if (lobbyResponse.genericResponse.success) {}

      return lobbyResponse;
    });
  }

  static Future<LobbyResponse> createLobby(String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/lobby/create_lobby",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      LobbyResponse lobbyResponse =
          LobbyResponse.fromJson(json.decode(response.body));

      if (lobbyResponse.genericResponse.success) {
        await getProfile(token);
      }

      return lobbyResponse;
    });
  }

  static Future<GenericResponse> exitLobby(String token) async {
    return http.post(
      Constants.API_BASE_URL + "/lobby/exit_lobby",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) {
        await getProfile(token);
      }

      return genericResponse;
    });
  }

  static Future<GenericResponse> joinLobby(String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/lobby/join_lobby",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      GenericResponse genericResponse =
          GenericResponse.fromJson(json.decode(response.body));

      if (genericResponse.success) {
        await getProfile(token);
      }

      return genericResponse;
    });
  }

  static Future<GamesResponse> getGames(String token) async {
    return http.post(
      Constants.API_BASE_URL + "/game/get_games",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      GamesResponse gamesResponse =
          GamesResponse.fromJson(json.decode(response.body));

      if (gamesResponse.genericResponse.success) {}

      return gamesResponse;
    });
  }

  static Future<InboxResponse> getInbox(String token) async {
    return http.post(
      Constants.API_BASE_URL + "/user/get_inbox",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
    ).then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      InboxResponse inboxResponse =
          InboxResponse.fromJson(json.decode(response.body));

      if (inboxResponse.genericResponse.success) {}

      return inboxResponse;
    });
  }

  static Future<ConversationResponse> getConversation(
      String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/user/get_conversation",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      ConversationResponse conversationResponse =
          ConversationResponse.fromJson(json.decode(response.body));

      if (conversationResponse.genericResponse.success) {}

      return conversationResponse;
    });
  }

  static Future<ProfileResponse> getUserByUsername(
      String token, Map body) async {
    return http
        .post(
      Constants.API_BASE_URL + "/user/get_user_by_username",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "JWT " + token,
      },
      body: json.encode(body),
    )
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data. code: " + statusCode.toString());
      }
      ProfileResponse profileResponse =
          ProfileResponse.fromJson(json.decode(response.body));

      if (profileResponse.genericResponse.success) {}

      return profileResponse;
    });
  }
}
