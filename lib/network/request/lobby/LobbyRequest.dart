class LobbyRequest {
  String _lobbyId;

  LobbyRequest({String lobbyId}) {
    this._lobbyId = lobbyId;
  }

  String get lobbyId => _lobbyId;
  set lobbyId(String lobbyId) => _lobbyId = lobbyId;

  LobbyRequest.fromJson(Map<String, dynamic> json) {
    print("Lobbi Id => " + json.toString());
    _lobbyId = json['lobby_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lobby_id'] = this._lobbyId;
    return data;
  }
}
