class LobbiesRequest {
  String _gameId;

  LobbiesRequest({String gameId}) {
    this._gameId = gameId;
  }

  String get gameId => _gameId;
  set gameId(String type) => _gameId = type;

  LobbiesRequest.fromJson(Map<String, dynamic> json) {
    _gameId = json['game_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['game_id'] = this._gameId;
    return data;
  }
}
