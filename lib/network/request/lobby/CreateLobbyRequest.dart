class CreateLobbyRequest {
  String _gameId;
  int _memberLimit;
  String _rankName;

  CreateLobbyRequest({String gameId, int memberLimit, String rankName}) {
    this._gameId = gameId;
    this._memberLimit = memberLimit;
    this._rankName = rankName;

    toString();
  }

  String get gameId => _gameId;
  set gameId(String gameId) => _gameId = gameId;
  int get memberLimit => _memberLimit;
  set memberLimit(int memberLimit) => _memberLimit = memberLimit;
  String get rankName => _rankName;
  set rankName(String rankName) => _rankName = rankName;

  CreateLobbyRequest.fromJson(Map<String, dynamic> json) {
    _gameId = json['game_id'];
    _memberLimit = json['member_limit'];
    _rankName = json['rank_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['game_id'] = this._gameId;
    data['member_limit'] = this._memberLimit;
    data['rank_name'] = this._rankName;
    return data;
  }

  @override
  String toString() {
    return 'CreateLobbyRequest{_gameId: $_gameId, _memberLimit: $_memberLimit, _rankName: $_rankName}';
  }
}
