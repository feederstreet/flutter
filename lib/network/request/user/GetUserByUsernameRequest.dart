class GetUserByUsernameRequest {
  String username;

  GetUserByUsernameRequest(this.username);

  GetUserByUsernameRequest.fromJson(Map<String, dynamic> json) {
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.username != null) data['username'] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'ChangeSettingsRequest{username: $username}';
  }
}
