class ConversationRequest {
  String targetId;

  ConversationRequest(this.targetId);

  ConversationRequest.fromJson(Map<String, dynamic> json) {
    targetId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.targetId != null) data['user_id'] = this.targetId;
    return data;
  }

  @override
  String toString() {
    return 'ChangeSettingsRequest{targetId: $targetId}';
  }
}
