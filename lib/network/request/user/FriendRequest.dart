class FriendRequest {
  String targetId;

  FriendRequest(this.targetId);

  FriendRequest.fromJson(Map<String, dynamic> json) {
    targetId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.targetId != null) data['user_id'] = this.targetId;
    return data;
  }

  @override
  String toString() {
    return 'ChangeSettingsRequest{targetId: $targetId}';
  }
}
