class ChangeSettingsRequest {
  String id;
  String fullname;
  String username;
  bool gender;
  int birthDate;
  String email;
  String avatar;
  String password;
  String facebookUrl;
  String discordUrl;

  ChangeSettingsRequest({
    this.id,
    this.fullname,
    this.username,
    this.gender,
    this.birthDate,
    this.email,
    this.avatar,
  });

  ChangeSettingsRequest.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    password = json['password'];
    fullname = json['fullname'];
    username = json['username'];
    avatar = json['avatar'];
    gender = json['gender'];
    birthDate = json['birth_date'];
    email = json['email'];
    facebookUrl = json['facebook_url'];
    discordUrl = json['discord_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.id != null) data['_id'] = this.id;
    if (this.fullname != null) data['fullname'] = this.fullname;
    if (this.username != null) data['username'] = this.username;
    if (this.password != null) data['password'] = this.password;
    if (this.gender != null) data['gender'] = this.gender;
    if (this.birthDate != null) data['birth_date'] = this.birthDate;
    if (this.email != null) data['email'] = this.email;
    if (this.avatar != null) data['avatar'] = this.avatar;
    if (this.facebookUrl != null) data['facebook_url'] = this.facebookUrl;
    if (this.discordUrl != null) data['discord_url'] = this.discordUrl;
    return data;
  }

  @override
  String toString() {
    return 'ChangeSettingsRequest{id: $id, fullname: $fullname, username: $username, gender: $gender, birthDate: $birthDate, email: $email, avatar: $avatar, password: $password}';
  }
}
