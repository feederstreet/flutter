class SignWithFacebookRequest {
  String _email;
  String _fullname;
  String _facebook_id;

  SignWithFacebookRequest({String email, String facebook_id, String fullname}) {
    this._email = email;
    this._fullname = fullname;
    this._facebook_id = facebook_id;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get fullname => _fullname;

  set fullname(String value) {
    _fullname = value;
  }

  String get facebook_id => _facebook_id;

  set facebook_id(String value) {
    _facebook_id = value;
  }

  SignWithFacebookRequest.fromJson(Map<String, dynamic> json) {
    _fullname = json['_fullname'];
    _email = json['email'];
    _facebook_id = json['facebook_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this._email;
    data['fullname'] = this._fullname;
    data['facebook_id'] = this._facebook_id;
    return data;
  }

  @override
  String toString() {
    return 'SignWithFacebookRequest{_email: $_email, _fullname: $_fullname, _facebook_id: $_facebook_id}';
  }
}
