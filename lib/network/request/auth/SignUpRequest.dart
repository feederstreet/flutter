class SignUpRequest {
  String _email;
  String _username;
  String _password;

  SignUpRequest({String email, String password, String username}) {
    this._email = email;
    this._username = username;
    this._password = password;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get username => _username;

  set username(String value) {
    _username = value;
  }

  String get password => _password;

  set password(String value) {
    _password = value;
  }

  SignUpRequest.fromJson(Map<String, dynamic> json) {
    _username = json['username'];
    _email = json['email'];
    _password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this._email;
    data['username'] = this._username;
    data['password'] = this._password;
    return data;
  }

  @override
  String toString() {
    return 'SignUpRequest{_email: $_email, _username: $_username, _password: $_password}';
  }
}
