class SignWithGoogleRequest {
  String _email;
  String _fullname;
  String _google_id;

  SignWithGoogleRequest({String email, String google_id, String fullname}) {
    this._email = email;
    this._fullname = fullname;
    this._google_id = google_id;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get fullname => _fullname;

  set fullname(String value) {
    _fullname = value;
  }

  String get google_id => _google_id;

  set google_id(String value) {
    _google_id = value;
  }

  SignWithGoogleRequest.fromJson(Map<String, dynamic> json) {
    _fullname = json['_fullname'];
    _email = json['email'];
    _google_id = json['google_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this._email;
    data['fullname'] = this._fullname;
    data['google_id'] = this._google_id;
    return data;
  }

  @override
  String toString() {
    return 'SignWithGoogleRequest{_email: $_email, _fullname: $_fullname, _google_id: $_google_id}';
  }
}
