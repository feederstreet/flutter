class SignInRequest {
  String _usernameOrEmail;
  String _password;

  SignInRequest({String usernameOrEmail, String password}) {
    this._usernameOrEmail = usernameOrEmail;
    this._password = password;
  }

  String get usernameOrEmail => _usernameOrEmail;
  set usernameOrEmail(String usernameOrEmail) =>
      _usernameOrEmail = usernameOrEmail;
  String get password => _password;
  set password(String password) => _password = password;

  SignInRequest.fromJson(Map<String, dynamic> json) {
    _usernameOrEmail = json['username_or_email'];
    _password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username_or_email'] = this._usernameOrEmail;
    data['password'] = this._password;
    return data;
  }

  @override
  String toString() {
    return 'SignInRequest{_usernameOrEmail: $_usernameOrEmail, _password: $_password}';
  }
}
