import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/ChangeSettingsRequest.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/presenters/SettingsPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/HomePage.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/FormatHelper.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/utils/ValidateHelper.dart';
import 'package:gamerhub/views/SettingsView.dart';

class SettingsPage extends StatefulWidget {
  SettingsPageState state;

  SettingsPage(bool _hasBeforePage, {Key key}) : super(key: key) {
    state = new SettingsPageState(_hasBeforePage);
  }

  SettingsPageState createState() {
    return state;
  }
}

class SettingsPageState extends State<SettingsPage>
    with SingleTickerProviderStateMixin
    implements SettingsView {
  SettingsPresenter presenter;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _hasBeforePage = false;

  final _key = new GlobalKey<FormState>();

  String photoUrl = "";

  var _fullNameController = new TextEditingController();

  /// male = 1, female = 0;
  int _gender = -1;
  var _birthdateController = new TextEditingController();
  var _birthdate = 0;

  var _userNameController = new TextEditingController();
  var _eMailController = new TextEditingController();

  SettingsPageState(bool _hasBeforePage) {
    this._hasBeforePage = _hasBeforePage;
    presenter = new SettingsPresenter(this);
  }

  @override
  void initState() {
    super.initState();
    presenter.initView();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "AYARLAR",
          style: Theme.Text.titleStyle(size: 18.0),
        ),
        centerTitle: true,
      ),
      body: Builder(
        builder: (context) =>
            NotificationListener<OverscrollIndicatorNotification>(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: Theme.Background.decoration,
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 40, vertical: 20),
                          child: InkWell(
                            onTap: () => onClickImage(context),
                            child: Container(
                                width: 150.0,
                                height: 150.0,
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    image: DecorationImage(
                                        image: this.photoUrl.isNotEmpty
                                            ? NetworkImage(this.photoUrl)
                                            : Theme.PlaceHolder.user,
                                        fit: BoxFit.fill),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(75.0)),
                                    boxShadow: [
                                      BoxShadow(
                                          blurRadius: 5.0, color: Colors.black)
                                    ])),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          child: Column(
                            children: <Widget>[
                              Form(
                                key: _key,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Card(
                                      elevation: Theme.Card.elevation,
                                      color: Theme.Card.backgroundColor,
                                      shape: Theme.Card.shape,
                                      child: Container(
                                        width: double.infinity,
                                        child: Padding(
                                          padding:
                                              EdgeInsets.only(bottom: 10.0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 10,
                                                    horizontal: 10),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Align(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Text(
                                                        "KİŞİSEL BİLGİLER",
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: Theme.Text
                                                            .headerStyle(
                                                                size: 16.0),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Theme.Line.both(double.infinity),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20),
                                                child: TextFormField(
                                                  controller:
                                                      _fullNameController,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  validator: (val) =>
                                                      ValidateHelper.name(val),
                                                  onSaved: (val) =>
                                                      _fullNameController.text =
                                                          val,
                                                  inputFormatters: [
                                                    new UpperCaseTextFormatter(),
                                                  ],
                                                  style:
                                                      Theme.TextField.textStyle,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    labelText: "Tam İsmi",
                                                    labelStyle: Theme
                                                        .TextField.labelStyle,
                                                    errorStyle: Theme
                                                        .TextField.errorStyle,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  new Radio(
                                                    value: 1,
                                                    groupValue: _gender,
                                                    onChanged: (val) =>
                                                        setState(() {
                                                          _gender = val;
                                                        }),
                                                  ),
                                                  new Text('Erkek',
                                                      style: Theme
                                                          .TextField.textStyle),
                                                  new Radio(
                                                    value: 0,
                                                    groupValue: _gender,
                                                    onChanged: (val) =>
                                                        setState(() {
                                                          _gender = val;
                                                        }),
                                                  ),
                                                  new Text('Kadın',
                                                      style: Theme
                                                          .TextField.textStyle)
                                                ],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20),
                                                child: DateTimePickerFormField(
                                                  controller:
                                                      _birthdateController,
                                                  inputType: InputType.date,
                                                  format:
                                                      DateFormatter.dateFormat,
                                                  editable: true,
                                                  validator: (val) {
                                                    return ValidateHelper.date(
                                                        _birthdateController
                                                            .text);
                                                  },
                                                  onSaved: (val) =>
                                                      setState(() {
                                                        if (val != null) {
                                                          _birthdateController
                                                                  .text =
                                                              DateFormatter
                                                                  .date(val);

                                                          _birthdate =
                                                              val.millisecond;
                                                        }
                                                      }),
                                                  style:
                                                      Theme.TextField.textStyle,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    labelText: 'Doğum Tarihi',
                                                    labelStyle: Theme
                                                        .TextField.labelStyle,
                                                    errorStyle: Theme
                                                        .TextField.errorStyle,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Card(
                                      elevation: Theme.Card.elevation,
                                      color: Theme.Card.backgroundColor,
                                      shape: Theme.Card.shape,
                                      child: Container(
                                        width: double.infinity,
                                        child: Padding(
                                          padding:
                                              EdgeInsets.only(bottom: 10.0),
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 10,
                                                    horizontal: 10),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Align(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Text(
                                                        "HESAP BİLGİLERİ",
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: Theme.Text
                                                            .headerStyle(
                                                                size: 16.0),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Theme.Line.both(double.infinity),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20),
                                                child: TextFormField(
                                                  controller:
                                                      _userNameController,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  validator: (val) =>
                                                      ValidateHelper.userName(
                                                          val),
                                                  onSaved: (val) =>
                                                      _userNameController.text =
                                                          val,
                                                  style:
                                                      Theme.TextField.textStyle,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    labelText: "Kullanıcı Adı",
                                                    labelStyle: Theme
                                                        .TextField.labelStyle,
                                                    errorStyle: Theme
                                                        .TextField.errorStyle,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20),
                                                child: TextFormField(
                                                  controller: _eMailController,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  validator: (val) =>
                                                      ValidateHelper.eMail(val),
                                                  onSaved: (val) =>
                                                      _eMailController.text =
                                                          val,
                                                  textCapitalization:
                                                      TextCapitalization.words,
                                                  style:
                                                      Theme.TextField.textStyle,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    labelText: "E Posta",
                                                    labelStyle: Theme
                                                        .TextField.labelStyle,
                                                    errorStyle: Theme
                                                        .TextField.errorStyle,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
      ),
      bottomNavigationBar: Container(
        decoration: new BoxDecoration(
          color: Theme.Button.backgroundColor,
        ),
        child: MaterialButton(
          elevation: Theme.Button.elevation,
          minWidth: double.infinity,
          onPressed: () => _saved(context),
          child: Container(
            padding: EdgeInsets.all(14.0),
            child: Text(
              "Kaydet",
              style: Theme.Button.textStyle,
            ),
          ),
        ),
      ),
    );
  }

  Future onClickImage(BuildContext context) async {
    //TODO:Fotoğraf Değiştirme.
    presenter.changeProfilPhoto();
  }

  Future _saved(BuildContext context) async {
    final form = _key.currentState;

    if (form.validate()) {
      form.save();

      var user = await SharedPreferencesHelper.getUser();
      ChangeSettingsRequest request = new ChangeSettingsRequest();
      if (this._fullNameController.text != user.fullname)
        request.fullname = this._fullNameController.text;
      if ((this._gender == 1 ? true : false) != user.gender)
        request.gender = this._gender == 1 ? true : false;
      if (DateFormatter.dateToMil(this._birthdateController.text) !=
          user.birthDate) {
        request.birthDate =
            DateFormatter.dateToMil(this._birthdateController.text);
      }
      if (this._userNameController.text != user.username)
        request.username = this._userNameController.text;
      if (this._eMailController.text != user.email)
        request.email = this._eMailController.text;

      changeSettings(request);
    }
  }

  changeSettings(ChangeSettingsRequest request) async {
    if (request != null) {
      GenericResponse response = await Api.changeSettings(
          await SharedPreferencesHelper.getToken(), request.toJson());

      if (response.success) {
        return await onResponse(
            (await Api.getProfile(await SharedPreferencesHelper.getToken()))
                .user);
      } else {
        return onFailure(response.error);
      }
    }
  }

  @override
  void refreshImage(String photoUrl) {
    setState(() {
      if (photoUrl != null) {
        if (photoUrl.isNotEmpty) this.photoUrl = photoUrl;
      }
    });
  }

  @override
  void refreshData(User user) {
    setState(() {
      if (user.avatar != null) {
        if (user.avatar.isNotEmpty) this.photoUrl = user.avatar;
      }
      this._fullNameController.text = user.fullname;
      if (user.gender != null) {
        this._gender = (user.gender ? 1 : 0);
      }
      this._birthdateController.text =
          user.birthDate == null ? "" : DateFormatter.dateByMil(user.birthDate);
      this._birthdate = user.birthDate == null ? 0 : user.birthDate;
      this._userNameController.text = user.username;
      this._eMailController.text = user.email;

      final form = _key.currentState;
      form.save();
    });
  }

  @override
  void onFailure(ErrorResponse error) {
    if (error.code != -1)
      UIHelper.showSnackBarByKey(_scaffoldKey, error.text,
          duration: UIHelper.SHORT_TIME);
    else {
      if (!_hasBeforePage) {
        SharedPreferencesHelper.putBoolean(true, Constants.SP_SETTINGS_KEY);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => HomePage(), fullscreenDialog: false),
            (Route<dynamic> route) => false);
      } else {
        UIHelper.showSnackBarByKey(_scaffoldKey, "Bir Değişiklik yapınız",
            duration: UIHelper.SHORT_TIME);
      }
    }
  }

  @override
  Future onResponse(User user) async {
    if (!_hasBeforePage) {
      SharedPreferencesHelper.putBoolean(true, Constants.SP_SETTINGS_KEY);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => HomePage(), fullscreenDialog: false),
          (Route<dynamic> route) => false);
    } else
      Navigator.of(context).pop(true);
  }
}
