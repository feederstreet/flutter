import 'package:flutter/material.dart';
import 'package:gamerhub/model/Conversation.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/presenters/ChatPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/views/ChatView.dart';

class ChatPage extends StatefulWidget {
  User _target;

  ChatPage(this._target);

  ChatPageState createState() {
    return new ChatPageState(this._target);
  }
}

class ChatPageState extends State<ChatPage>
    with TickerProviderStateMixin
    implements ChatView {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _messageController = new TextEditingController();
  ChatPresenter _presenter;

  User _target;

  List<Conversation> _conversation = new List();

  ChatPageState(this._target) {
    _presenter = new ChatPresenter(this);
  }

  @override
  void initState() {
    getConversation();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 16),
              width: 36.0,
              height: 36.0,
              decoration: BoxDecoration(
                color: Colors.grey,
                image: DecorationImage(
                    image: this._target.avatar != null
                        ? NetworkImage(this._target.avatar)
                        : Theme.PlaceHolder.user,
                    fit: BoxFit.contain),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
              ),
            ),
            new Expanded(
              flex: 1,
              child: new Text(
                this._target.username,
                style: Theme.Text.textStyle(),
              ),
            ),
          ],
        ),
      ),
      body: new Container(
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Container(
                child: new ListView.builder(
                  padding: new EdgeInsets.all(8.0),
                  reverse: true,
                  itemBuilder: (_, int index) {
                    return new Row(
                      children: <Widget>[
                        _conversation[index].userId != _target.id
                            ? Expanded(child: new Container())
                            : new Container(),
                        new Card(
                            elevation: Theme.Card.elevation,
                            shape: Theme.Card.shape,
                            child: new Container(
                              padding: EdgeInsets.all(8),
                              child: new Row(
                                mainAxisAlignment:
                                    _conversation[index].userId == _target.id
                                        ? MainAxisAlignment.start
                                        : MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _conversation[index].userId == _target.id
                                      ? new Container(
                                          child: new Text(
                                            _target.username + ": ",
                                            style: Theme.Text.headerStyle(),
                                          ),
                                        )
                                      : new Container(),
                                  new Container(
                                    child: new Text(
                                      _conversation[index].message,
                                      softWrap: true,
                                      style: new TextStyle(
                                          color: Theme.Text.headerStyle().color,
                                          fontSize: 16),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                        _conversation[index].userId == _target.id
                            ? Expanded(child: new Container())
                            : new Container(),
                      ],
                    );
                  },
                  itemCount: _conversation.length,
                ),
              ),
            ),
            new Card(
              margin: EdgeInsets.all(0.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
              child: new Container(
                padding: EdgeInsets.only(left: 10, right: 5),
                child: new Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: _messageController,
                        style: Theme.TextField.textStyle,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Bir Mesaj Yazınız..",
                            hintStyle: Theme.TextField.labelStyle),
                      ),
                    ),
                    IconButton(
                        icon: new Icon(
                          Icons.send,
                          color: Theme.Colors.accent,
                          size: 24,
                        ),
                        onPressed: presSendButton),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void presSendButton() {
    if (_messageController != null) {
      if (_messageController.text != null) {
        if (_messageController.text.isNotEmpty) {
          _presenter.sendMessage(_target.id, _messageController.text);
          return;
        }
      }
    }
    UIHelper.showSnackBarByKey(_scaffoldKey, "Bir mesaj yazınız!",
        duration: UIHelper.SHORT_TIME);
  }

  void getConversation() {
    _presenter.getConversation(_target.id).then((list) {
      _conversation.clear();
      setState(() {
        _conversation = list;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
