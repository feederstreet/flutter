import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/response/FriendsResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Loader.dart';
import 'package:gamerhub/ui/Pages/OtherUserPage.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class LobbyMessagePage extends StatefulWidget {
  BuildContext _parentContext;

  LobbyMessagePage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new LobbyMessageListView(_parentContext);
  }
}

class LobbyMessageListView extends State<LobbyMessagePage> {
  BuildContext _parentContext;
  final _key = new GlobalKey<FormState>();

  List<User> _friends = new List();

  var _asyncLoaderState = new GlobalKey<AsyncLoaderState>();

  LobbyMessageListView(this._parentContext);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: RefreshIndicator(
            child: Container(
              decoration: Theme.Background.decoration,
              child: Form(
                key: _key,
                child: new Column(
                  children: <Widget>[
                    new AsyncLoader(
                      key: _asyncLoaderState,
                      initState: () => refreshLobiMessages(),
                      renderLoad: () => Expanded(
                            child: Center(
                              child: new Loader(
                                dotOneColor: Colors.blue,
                                dotTwoColor: Colors.red,
                                dotThreeColor: Colors.yellow,
                                dotType: DotType.circle,
                              ),
                            ),
                          ),
                      renderError: ([error]) => new Expanded(
                            child: new Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 16.0),
                                decoration: Theme.Button.boxDecoration,
                                child: MaterialButton(
                                  elevation: Theme.Button.elevation,
                                  onPressed: () => _asyncLoaderState
                                      .currentState
                                      .reloadState(),
                                  child: Container(
                                    padding: EdgeInsets.all(14.0),
                                    child: Text(
                                      "Tekrar Dene",
                                      style: Theme.Button.textStyle,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                      renderSuccess: ({data}) => new Expanded(
                            child: Container(
                              child: (data != null && _friends.isNotEmpty)
                                  ? ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      itemCount: _friends.length,
                                      padding: const EdgeInsets.all(5.0),
                                      itemBuilder: (context, position) {
                                        return GestureDetector(
                                          child: new Card(
                                            elevation: Theme.Card.elevation,
                                            shape: Theme.Card.shape,
                                            child: new Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 8.0,
                                                  horizontal: 8.0),
                                              child: new Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 16),
                                                    width: 64.0,
                                                    height: 64.0,
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey,
                                                      image: DecorationImage(
                                                          image: _friends[position]
                                                                      .avatar !=
                                                                  null
                                                              ? NetworkImage(this
                                                                  ._friends[
                                                                      position]
                                                                  .avatar)
                                                              : Theme
                                                                  .PlaceHolder
                                                                  .user,
                                                          fit: BoxFit.contain),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  75.0)),
                                                    ),
                                                  ),
                                                  new Expanded(
                                                    flex: 1,
                                                    child: new Text(
                                                      _friends[position]
                                                          .username,
                                                      style: Theme.Text
                                                          .textStyle(),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: () => goToMessagePage(
                                              _friends[position]),
                                        );
                                      })
                                  : new Container(
                                      child: Center(
                                        child:
                                            Text("Mesajınız bulunmamaktadır!"),
                                      ),
                                    ),
                            ),
                          ),
                    )
                  ],
                ),
              ),
            ),
            onRefresh: _handleRefresh));
  }

  Future<Null> _handleRefresh() async {
    await _asyncLoaderState.currentState.reloadState();
    return null;
  }

  Future<FriendsResponse> refreshLobiMessages() async {
    _friends.clear();

    FriendsResponse response =
        await Api.getFriends(await SharedPreferencesHelper.getToken());
    if (response.genericResponse.success) {
      if (response.friends != null)
        response.friends.length > 0
            ? _friends = response.friends
            : _friends = new List<User>();
    }
    await Future.delayed(Duration(seconds: 1));
    return response;
  }

  void goToMessagePage(User user) {
    Navigator.of(_parentContext).push(new MaterialPageRoute(
        builder: (BuildContext context) => new OtherUserPage(user.id)));
  }
}
