import 'dart:async';

import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/presenters/LobbiesPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/ListViews/GameImagesListView.dart';
import 'package:gamerhub/ui/ListViews/LobiesListView.dart';
import 'package:gamerhub/ui/Loader.dart';
import 'package:gamerhub/ui/Pages/AddLobbyPage.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/views/LobbiesView.dart';

class LobisPage extends StatefulWidget {
  BuildContext _parentContext;

  LobisPage(this._parentContext);

  @override
  LobisPageState createState() {
    return new LobisPageState(_parentContext);
  }
}

class LobisPageState extends State<LobisPage> implements LobbiesView {
  LobbiesPresenter presenter;
  BuildContext _parentContext;

  List<Lobby> lobbies = new List<Lobby>();
  List<Game> games = new List<Game>();
  String selectGameId = "";
  var _lobisLoaderState = new GlobalKey<AsyncLoaderState>();

  LobisPageState(this._parentContext) {
    presenter = new LobbiesPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: new Text(
          "LOBİLER",
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          child: Container(
            decoration: Theme.Background.decoration,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                this.games.length > 0
                    ? new GameImagesListView(_parentContext, this, this.games)
                    : new Container(),
                new AsyncLoader(
                  key: _lobisLoaderState,
                  initState: () => refreshLobies(selectGameId),
                  renderLoad: () => Expanded(
                        child: Center(
                          child: new Loader(
                            dotType: DotType.circle,
                          ),
                        ),
                      ),
                  renderError: ([error]) => new Expanded(
                        child: new Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 16.0),
                            decoration: Theme.Button.boxDecoration,
                            child: MaterialButton(
                              elevation: Theme.Button.elevation,
                              onPressed: () => _handleRefresh(),
                              child: Container(
                                padding: EdgeInsets.all(14.0),
                                child: Text(
                                  "Tekrar Dene",
                                  style: Theme.Button.textStyle,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                  renderSuccess: ({data}) => this.lobbies.length > 0
                      ? new Expanded(
                          child: new LobiesListView(_parentContext, data),
                        )
                      : new Expanded(
                          child: new Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Lobi Bulunmamaktadır!",
                                  style: Theme.Text.titleStyle(),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 16.0),
                                  decoration: Theme.Button.boxDecoration,
                                  child: MaterialButton(
                                    elevation: Theme.Button.elevation,
                                    onPressed: () => _handleRefresh(),
                                    child: Container(
                                      padding: EdgeInsets.all(14.0),
                                      child: Text(
                                        "Tekrar Dene",
                                        style: Theme.Button.textStyle,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                )
              ],
            ),
          ),
          onRefresh: _handleRefresh),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () => addLobi(),
      ),
    );
  }

  @override
  void didUpdateWidget(LobisPage oldWidget) {
    _lobisLoaderState.currentState.reloadState();
  }

  Future _handleRefresh() async {
    await _lobisLoaderState.currentState.reloadState();
    return;
  }

  @override
  Future addLobi() async {
    if (await presenter.hasMyLobby()) {
      UIHelper.showSnackBarByContext(
          _parentContext, "Mevut Lobinizi kapatmanız gerekiyor!");
    } else {
      Navigator.of(_parentContext).push(new MaterialPageRoute(
          builder: (BuildContext context) =>
              new SelectGameForLobbyPage(_parentContext)));
    }
  }

  @override
  void goToFilter() {
    // TODO: implement goToFilter
    return null;
  }

  @override
  Future<List<Game>> refreshGames() async {
    this.games = new List();
    List<Game> games = await presenter.getGames();
    setState(() {
      this.games = games;
    });
    return this.games;
  }

  @override
  Future<List<Lobby>> refreshLobies(String gameId) async {
    await refreshGames();

    if (gameId.isEmpty) {
      if (this.games.length > 0) {
        selectGameId = this.games[0].sId;
      } else {
        selectGameId = "";
      }
    } else {
      selectGameId = gameId;
    }

    this.lobbies = new List();
    List<Lobby> lobbies = new List();

    if (selectGameId.isNotEmpty) {
      lobbies = await presenter.getLobbies(selectGameId);
    }

    this.lobbies = lobbies;
    await Future.delayed(Duration(seconds: 1));
    return this.lobbies;
  }

  @override
  void selectGame(String gameId) {
    selectGameId = gameId;
    this._handleRefresh();
  }
}
