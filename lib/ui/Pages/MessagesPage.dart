import 'package:flutter/material.dart';
import 'package:gamerhub/ui/Pages/PersonMessagePage.dart';

class MessagesPage extends StatefulWidget {
  BuildContext _parentContext;

  MessagesPage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new MessagesPageState(_parentContext);
  }
}

class MessagesPageState extends State<MessagesPage> {
  BuildContext _parentContext;

  MessagesPageState(this._parentContext);

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: new Text(
          "MESAJLAR",
        ),
        centerTitle: true,
      ),
      body: new PersonMessagePage(_parentContext),
    );
  }
}
