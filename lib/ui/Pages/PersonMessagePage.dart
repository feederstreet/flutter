import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/Message.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/response/InboxResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Loader.dart';
import 'package:gamerhub/ui/Pages/ChatPage.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class PersonMessagePage extends StatefulWidget {
  BuildContext _parentContext;

  PersonMessagePage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new PersonMessageListView(_parentContext);
  }
}

class PersonMessageListView extends State<PersonMessagePage> {
  BuildContext _parentContext;
  final _key = new GlobalKey<FormState>();

  List<Message> _messages = new List();

  var _asyncLoaderState = new GlobalKey<AsyncLoaderState>();

  PersonMessageListView(this._parentContext);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: RefreshIndicator(
            child: Container(
              decoration: Theme.Background.decoration,
              child: Form(
                key: _key,
                child: new Column(
                  children: <Widget>[
                    new AsyncLoader(
                      key: _asyncLoaderState,
                      initState: () => refreshPersonMessages(),
                      renderLoad: () => Expanded(
                            child: Center(
                              child: new Loader(
                                dotOneColor: Colors.blue,
                                dotTwoColor: Colors.red,
                                dotThreeColor: Colors.yellow,
                                dotType: DotType.circle,
                              ),
                            ),
                          ),
                      renderError: ([error]) => new Expanded(
                            child: new Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 16.0),
                                decoration: Theme.Button.boxDecoration,
                                child: MaterialButton(
                                  elevation: Theme.Button.elevation,
                                  onPressed: () => _asyncLoaderState
                                      .currentState
                                      .reloadState(),
                                  child: Container(
                                    padding: EdgeInsets.all(14.0),
                                    child: Text(
                                      "Tekrar Dene",
                                      style: Theme.Button.textStyle,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                      renderSuccess: ({data}) => new Expanded(
                            child: Container(
                              child: (data != null && _messages.isNotEmpty)
                                  ? ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      itemCount: _messages.length,
                                      padding: const EdgeInsets.all(5.0),
                                      itemBuilder: (context, position) {
                                        return GestureDetector(
                                          child: new Card(
                                              elevation: Theme.Card.elevation,
                                              shape: Theme.Card.shape,
                                              child: new Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  new Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: <Widget>[
                                                      new Container(
                                                        child: new Text(
                                                          _messages[position]
                                                              .username,
                                                          style: Theme.Text
                                                              .headerStyle(),
                                                        ),
                                                        padding:
                                                            EdgeInsets.only(
                                                          top: 16,
                                                          left: 16,
                                                          right: 16,
                                                        ),
                                                      ),
                                                      new Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 16,
                                                                left: 16,
                                                                right: 16),
                                                        margin: EdgeInsets.only(
                                                            top: 4),
                                                        child: new Text(
                                                          _messages[position]
                                                              .content,
                                                          style: new TextStyle(
                                                              color: Theme.Text
                                                                      .headerStyle()
                                                                  .color,
                                                              fontSize: 16),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  new Container(
                                                    padding: EdgeInsets.all(16),
                                                    child: Theme.Icon.instance(
                                                        Icons.chevron_right),
                                                  )
                                                ],
                                              )),
                                          onTap: () => goToMessagePage(
                                              _messages[position]),
                                        );
                                      })
                                  : new Container(
                                      child: Center(
                                        child: Text(
                                          "Mesajınız bulunmamaktadır!",
                                          textAlign: TextAlign.center,
                                          style: Theme.Text.headerStyle(),
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                    )
                  ],
                ),
              ),
            ),
            onRefresh: _handleRefresh));
  }

  Future<Null> _handleRefresh() async {
    await _asyncLoaderState.currentState.reloadState();
    return null;
  }

  Future<InboxResponse> refreshPersonMessages() async {
    _messages.clear();

    InboxResponse response =
        (await Api.getInbox(await SharedPreferencesHelper.getToken()));
    if (response.genericResponse.success) {
      if (response.inbox != null)
        response.inbox.length > 0
            ? _messages = response.inbox
            : _messages = new List<Message>();
    }
    await Future.delayed(Duration(seconds: 1));
    return response;
  }

  void goToMessagePage(Message message) async {
    Api.getFriendProfile(await SharedPreferencesHelper.getToken(), message.sId)
        .then((response) {
      if (response.genericResponse.success) {
        Navigator.of(_parentContext).push(new MaterialPageRoute(
            builder: (BuildContext context) => new ChatPage(response.user)));
      }
    });
  }
}
