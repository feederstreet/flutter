import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/network/socket/Socket.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/LobbiesPage.dart';
import 'package:gamerhub/ui/Pages/MessagesPage.dart';
import 'package:gamerhub/ui/Pages/ProfilPage.dart';

class HomePage extends StatefulWidget {
  HomePageState createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage>
    with TickerProviderStateMixin
    implements SocketListener {
  PageController _controller;
  int page = 2;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  HomePageState() {
    SocketHelper().addListener(this);
    //SocketHelper().connect();
  }

  @override
  initState() {
    super.initState();
    _controller = new PageController(initialPage: this.page);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Builder(
        builder: (context) =>
            NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overScroll) {
                overScroll.disallowGlow();
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height >= 775.0
                    ? MediaQuery.of(context).size.height
                    : 775.0,
                decoration: Theme.Background.decoration,
                child: new PageView(
                    controller: _controller,
                    onPageChanged: (page) {
                      setState(() {
                        this.page = page;
                      });
                    },
                    children: <Widget>[
                      new MessagesPage(context),
                      new LobisPage(context),
                      new ProfilPage(context),
                    ]),
              ),
            ),
      ),
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (index) => {
              _controller.animateToPage(index,
                  duration: const Duration(milliseconds: 1000),
                  curve: Curves.ease)
            },
        currentIndex: page,
        items: [
          new BottomNavigationBarItem(
            title: new Container(height: 0.0),
            icon: new Icon(
              Icons.mail,
              size: 25.0,
            ),
          ),
          new BottomNavigationBarItem(
            title: new Container(height: 0.0),
            icon: new Icon(Icons.group_work, size: 25.0),
          ),
          new BottomNavigationBarItem(
            title: new Container(height: 0.0),
            icon: new Icon(
              FontAwesomeIcons.user,
              size: 25.0,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void onDone() {
    // TODO: implement onDone
  }

  @override
  void onError() {
    // TODO: implement onError
  }

  @override
  void receiveMessage() {
    // TODO: implement receiveMessage
  }
}
