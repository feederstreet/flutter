import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/ChangeSettingsRequest.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/utils/ValidateHelper.dart';

class ChangeContactPage extends StatefulWidget {
  ChangeContactPageState createState() {
    return new ChangeContactPageState();
  }
}

class ChangeContactPageState extends State<ChangeContactPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _key = new GlobalKey<FormState>();

  var _facebookController = new TextEditingController();
  var _discordController = new TextEditingController();

  bool obscureTextPassword = true;
  bool obscureTextConfirmPassword = true;

  @override
  void initState() {
    super.initState();
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "İLETİŞİM LİNKİ EKLE",
          style: Theme.Text.titleStyle(size: 18.0),
        ),
        centerTitle: true,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: Theme.Background.decoration,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Form(
                  key: _key,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          controller: _facebookController,
                          keyboardType: TextInputType.text,
                          validator: (val) => ValidateHelper.contacts(val),
                          onSaved: (val) => _facebookController.text = val,
                          style: Theme.TextField.textStyle,
                          decoration: InputDecoration(
                            icon:
                                Theme.Icon.instance(FontAwesomeIcons.facebookF),
                            border: InputBorder.none,
                            labelText: "Facebook Url",
                            labelStyle: Theme.TextField.labelStyle,
                            errorStyle: Theme.TextField.errorStyle,
                          ),
                        ),
                      ),
                      Theme.Line.both(double.infinity),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          controller: _discordController,
                          keyboardType: TextInputType.text,
                          validator: (val) => ValidateHelper.contacts(val),
                          onSaved: (val) => _discordController.text = val,
                          style: Theme.TextField.textStyle,
                          decoration: InputDecoration(
                            icon: Theme.Icon.instance(FontAwesomeIcons.discord),
                            border: InputBorder.none,
                            labelText: "Discord Url",
                            labelStyle: Theme.TextField.labelStyle,
                            errorStyle: Theme.TextField.errorStyle,
                          ),
                        ),
                      ),
                      Theme.Line.both(double.infinity),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        decoration: new BoxDecoration(
          color: Theme.Button.backgroundColor,
        ),
        child: MaterialButton(
          elevation: Theme.Button.elevation,
          minWidth: double.infinity,
          onPressed: () => _saved(context),
          child: Container(
            padding: EdgeInsets.all(14.0),
            child: Text(
              "Kaydet",
              style: Theme.Button.textStyle,
            ),
          ),
        ),
      ),
    );
  }

  Future _saved(BuildContext context) async {
    final form = _key.currentState;

    if (form.validate()) {
      form.save();

      var user = await SharedPreferencesHelper.getUser();
      ChangeSettingsRequest request = new ChangeSettingsRequest();

      if (this._facebookController.text != user.facebookUrl) {
        if (this._facebookController != null) {
          if (this._facebookController.text != null) {
            if (this._facebookController.text.trim().isNotEmpty) {
              if (user.facebookUrl != this._facebookController.text.trim())
                request.facebookUrl = this._facebookController.text.trim();
            } else if (this._facebookController.text.trim().isEmpty) {
              if (user.facebookUrl != null) {
                if (user.facebookUrl.trim().isNotEmpty)
                  return sameInput("Linki (Facebook) Silemezsiniz !");
              }
            }
          }
        }
      }

      if (this._discordController.text != user.discordUrl) {
        if (this._discordController != null) {
          if (this._discordController.text != null) {
            if (this._discordController.text.trim().isNotEmpty) {
              print("asb");
              if (user.discordUrl != this._discordController.text.trim())
                request.discordUrl = this._discordController.text.trim();
            } else if (this._discordController.text.trim().isEmpty) {
              if (user.discordUrl != null) {
                if (user.discordUrl.trim().isNotEmpty)
                  return sameInput("Linki (Discord) Silemezsiniz !");
              }
            }
          }
        }
      }

      if (request != null) {
        GenericResponse response = await Api.changeSettings(
            await SharedPreferencesHelper.getToken(), request.toJson());

        if (response.success) {
          return await onResponse(
              (await Api.getProfile(await SharedPreferencesHelper.getToken()))
                  .user);
        } else {
          return onFailure(response.error);
        }
      }
    }
  }

  Future refreshData() async {
    User user = await SharedPreferencesHelper.getUser();
    setState(() {
      if (user.facebookUrl != null) {
        if (user.facebookUrl.isNotEmpty)
          this._facebookController.text = user.facebookUrl;
      }
      if (user.discordUrl != null) {
        if (user.discordUrl.isNotEmpty)
          this._discordController.text = user.discordUrl;
      }

      final form = _key.currentState;
      form.save();
    });
  }

  void onFailure(ErrorResponse error) {
    UIHelper.showSnackBarByKey(_scaffoldKey,
        error.code != -1 ? error.text : "Bir Değişiklik Yapınız !",
        duration: UIHelper.SHORT_TIME);
  }

  Future sameInput(text) {
    UIHelper.showSnackBarByKey(_scaffoldKey, text,
        duration: UIHelper.SHORT_TIME);
  }

  Future onResponse(User user) async {
    Navigator.of(context).pop(true);
  }
}
