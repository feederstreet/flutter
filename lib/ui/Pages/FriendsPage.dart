import 'package:flutter/material.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/FriendRequest.dart';
import 'package:gamerhub/network/request/user/GetUserByUsernameRequest.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/ListViews/FriendRequestsListView.dart';
import 'package:gamerhub/ui/ListViews/FriendsListView.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';

class FriendsPage extends StatefulWidget {
  int count;

  FriendsPage(this.count);

  FriendsPageState createState() {
    return new FriendsPageState(count);
  }
}

class FriendsPageState extends State<FriendsPage>
    with TickerProviderStateMixin {
  int count = 0;
  TextEditingController _textFieldController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  FriendsPageState(this.count);

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
        length: 2,
        initialIndex: count > 0 ? 1 : 0,
        child: new Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
            title: new Text(
              "ARKADAŞLAR",
              style: Theme.Text.titleStyle(size: 18.0),
            ),
            actions: <Widget>[
              new IconButton(
                icon: new Icon(
                  Icons.person_add,
                ),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text(
                            'Arkadaş Ekle',
                            style: Theme.Text.titleStyle(),
                          ),
                          content: TextField(
                            controller: _textFieldController,
                            decoration: InputDecoration(
                                hintText: "Kullanıcı adı giriniz"),
                          ),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text('Ekle'),
                              onPressed: () => addFriend(
                                  context, _textFieldController.text.trim()),
                            )
                          ],
                        );
                      });
                },
              )
            ],
            centerTitle: true,
            bottom: new TabBar(
              labelStyle: Theme.Text.titleStyle(),
              labelColor: Theme.Colors.primary,
              unselectedLabelColor: Theme.Colors.primary,
              tabs: <Widget>[
                new Tab(
                  text: "Arkadaşlar",
                ),
                new Tab(
                  text: "İstekler",
                ),
              ],
            ),
          ),
          body: new TabBarView(children: <Widget>[
            new Container(child: new Friends(this.context)),
            new Container(child: new FriendRequests(this.context)),
          ]),
        ));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void addFriend(BuildContext context, String username) async {
    Api.getUserByUsername(await SharedPreferencesHelper.getToken(),
            new GetUserByUsernameRequest(username).toJson())
        .then((profileResponse) async {
      if (profileResponse.genericResponse.success) {
        GenericResponse response = await Api.addFriend(
            await SharedPreferencesHelper.getToken(),
            new FriendRequest(profileResponse.user.id).toJson());

        if (response.success) {
          Navigator.of(context).pop();
          UIHelper.showSnackBarByKey(
            _scaffoldKey,
            "Arkadaşlık isteği gönderildi !",
          );
        } else {
          Navigator.of(context).pop();
          UIHelper.showSnackBarByKey(
            _scaffoldKey,
            "Arkadaş eklenemedi !",
          );
        }
      } else {
        Navigator.of(context).pop();
        UIHelper.showSnackBarByKey(
          _scaffoldKey,
          "Arkadaş bulunamadı!",
        );
      }
    }).catchError(() {
      UIHelper.showSnackBarByKey(
        _scaffoldKey,
        "Hata Oluştu !",
      );
    });
  }
}
