import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/SignInPage.dart';
import 'package:gamerhub/ui/Pages/SignUpPage.dart';
import 'package:gamerhub/utils/UIHelper.dart';

// ignore: must_be_immutable
class SignPage extends StatefulWidget {
  SignPageState createState() {
    return new SignPageState();
  }
}

class SignPageState extends State<SignPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  PageController _pageController;

  Color left = Colors.black;
  Color right = Colors.white;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: Scaffold(
          key: _scaffoldKey,
          body: Builder(
            builder: (context) =>
                NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overScroll) {
                    overScroll.disallowGlow();
                  },
                  child: SingleChildScrollView(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height >= 775.0
                          ? MediaQuery.of(context).size.height
                          : 775.0,
                      decoration: Theme.Background.decoration,
                      child: SafeArea(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 40, vertical: 20),
                              child: Image.asset(
                                "assets/icon/isimli.png",
                                width: 200,
                                height: 200,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 40),
                              child: _buildMenuBar(context),
                            ),
                            Expanded(
                              child: PageView(
                                controller: _pageController,
                                onPageChanged: (i) {
                                  if (i == 0) {
                                    setState(() {
                                      right = Colors.white;
                                      left = Theme.Text.textColor;
                                    });
                                  } else if (i == 1) {
                                    setState(() {
                                      right = Theme.Text.textColor;
                                      left = Colors.white;
                                    });
                                  }
                                },
                                children: <Widget>[
                                  ConstrainedBox(
                                    constraints: BoxConstraints.expand(),
                                    child: new SignInPage(context),
                                  ),
                                  ConstrainedBox(
                                    constraints: BoxConstraints.expand(),
                                    child: new SignUpPage(context),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
          ),
        ),
        onWillPop: () => _willPopCallback());
  }

  Future<bool> _willPopCallback() async {
    return true; // return true if the route to be popped
  }

  /* Menü Bar */
  Widget _buildMenuBar(BuildContext context) {
    return Container(
      width: 300.0,
      height: 48.0,
      decoration: BoxDecoration(
        color: Theme.Colors.hint,
        borderRadius: BorderRadius.all(
          Radius.circular(25.0),
        ),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Giriş Yap",
                  style: TextStyle(color: left, fontSize: 14.0),
                ),
              ),
            ),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Kayıt Ol",
                  style: TextStyle(color: right, fontSize: 14.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.jumpToPage(0);
  }

  void _onSignUpButtonPress() {
    _pageController.jumpToPage(1);
  }
}
