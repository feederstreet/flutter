import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/ChangeContact.dart';
import 'package:gamerhub/ui/Pages/ChangePasswordPage.dart';
import 'package:gamerhub/ui/Pages/SettingsPage.dart';
import 'package:gamerhub/ui/Pages/SignPage.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class ProfilSettingPage extends StatefulWidget {
  ProfilSettingPageState createState() {
    return new ProfilSettingPageState();
  }
}

class ProfilSettingPageState extends State<ProfilSettingPage>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(),
      body: new Container(
        width: double.infinity,
        height: double.infinity,
        decoration: Theme.Background.decoration,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            FlatButton(
              onPressed: goToSettings,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding:
                        EdgeInsets.only(right: 8.0, top: 16.0, bottom: 16.0),
                    child: Theme.Icon.instance(Icons.settings),
                  ),
                  new Text(
                    "Ayarlar",
                    style: Theme.Text.textStyle(size: 18.0),
                  ),
                ],
              ),
            ),
            Theme.Line.both(double.infinity),
            FlatButton(
              onPressed: goToCommunications,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding:
                        EdgeInsets.only(right: 8.0, top: 16.0, bottom: 16.0),
                    child: Theme.Icon.instance(Icons.contacts),
                  ),
                  new Text(
                    "İletişim Linki Ekle",
                    style: Theme.Text.textStyle(size: 18.0),
                  ),
                ],
              ),
            ),
            Theme.Line.both(double.infinity),
            FlatButton(
              onPressed: goToChangePassword,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding:
                        EdgeInsets.only(right: 8.0, top: 16.0, bottom: 16.0),
                    child: Theme.Icon.instance(Icons.cached),
                  ),
                  new Text(
                    "Şifre Değiştir",
                    style: Theme.Text.textStyle(size: 18.0),
                  ),
                ],
              ),
            ),
            Theme.Line.both(double.infinity),
            FlatButton(
              onPressed: logOut,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    padding:
                        EdgeInsets.only(right: 8.0, top: 16.0, bottom: 16.0),
                    child: Icon(
                      Icons.exit_to_app,
                      color: Theme.Text.titleStyle().color,
                    ),
                  ),
                  new Text(
                    "Çıkış Yap",
                    style: Theme.Text.titleStyle(size: 18.0),
                  ),
                ],
              ),
            ),
            Theme.Line.both(double.infinity),
          ],
        ),
      ),
    );
  }

  void goToSettings() {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new SettingsPage(true)));
  }

  void goToCommunications() {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new ChangeContactPage()));
  }

  void goToChangePassword() {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new ChangePasswordPage()));
  }

  Future logOut() async {
    SharedPreferencesHelper.clearSharedPreference();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => SignPage(), fullscreenDialog: false),
        (Route<dynamic> route) => false);
  }
}
