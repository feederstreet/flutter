import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/presenters/SignInPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/SettingsPage.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/utils/ValidateHelper.dart';
import 'package:gamerhub/views/SignInView.dart';

class SignInPage extends StatefulWidget {
  BuildContext _parentContext;

  SignInPage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new SignInPageState(_parentContext);
  }
}

class SignInPageState extends State<SignInPage>
    with TickerProviderStateMixin
    implements SignInView {
  SignInPresenter presenter;
  final BuildContext _parentContext;

  final _key = new GlobalKey<FormState>();

  String _eMail;
  String _password;

  bool obscureTextSignInPassword = true;

  SignInPageState(this._parentContext) {
    presenter = new SignInPresenter(this);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 20.0, right: 40.0, left: 40.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              overflow: Overflow.visible,
              children: <Widget>[
                Card(
                    elevation: Theme.Card.elevation,
                    color: Theme.Card.backgroundColor,
                    shape: Theme.Card.shape,
                    child: Form(
                      key: _key,
                      child: Container(
                        width: double.infinity,
                        child: Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: Theme.TextField.padding,
                                child: TextFormField(
                                  keyboardType: TextInputType.emailAddress,
                                  style: Theme.TextField.textStyle,
                                  validator: (val) => ValidateHelper.eMail(val),
                                  onSaved: (val) => _eMail = val,
                                  decoration: InputDecoration(
                                    border: Theme.TextField.border,
                                    icon: Theme.Icon.instance(
                                      FontAwesomeIcons.envelope,
                                    ),
                                    labelText: "E-Posta Adresi",
                                    labelStyle: Theme.TextField.labelStyle,
                                    errorStyle: Theme.TextField.errorStyle,
                                  ),
                                ),
                              ),
                              Theme.Line.both(double.infinity),
                              Padding(
                                padding: Theme.TextField.padding,
                                child: TextFormField(
                                  obscureText: obscureTextSignInPassword,
                                  style: Theme.TextField.textStyle,
                                  validator: (val) =>
                                      ValidateHelper.password(val),
                                  onSaved: (val) => _password = val,
                                  decoration: InputDecoration(
                                    border: Theme.TextField.border,
                                    icon: Theme.Icon.instance(
                                      FontAwesomeIcons.lock,
                                    ),
                                    labelText: "Şifre",
                                    labelStyle: Theme.TextField.labelStyle,
                                    errorStyle: Theme.TextField.errorStyle,
                                    suffixIcon: GestureDetector(
                                      onTap: toggleSignInPassword,
                                      child: Theme.TextField.passwordIcon,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: Theme.Button.boxDecoration,
                                child: MaterialButton(
                                  elevation: Theme.Button.elevation,
                                  minWidth: double.infinity,
                                  onPressed: () => _signIn(),
                                  child: Container(
                                    padding: EdgeInsets.all(14.0),
                                    child: Text(
                                      "Giriş Yap",
                                      style: Theme.Button.textStyle,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: FlatButton(
                onPressed: () {},
                child: Text(
                  "Şifremi Unuttum?",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 18.0,
                      color: Theme.Text.hintColor,
                      fontFamily: Theme.Text.fontFamily),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Theme.Line.left(100.0),
                  Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Text(
                      "Ve Ya",
                      style: Theme.Text.labelStyle(size: 18.0),
                    ),
                  ),
                  Theme.Line.right(100.0),
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () => presenter.onSignInFaceBook(),
                        child: Theme.Icon.asSocial(FontAwesomeIcons.facebookF,
                            Theme.Colors.colorFacebook),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20.0),
                      child: GestureDetector(
                          onTap: () => presenter.onSignInGoogle(),
                          child: Theme.Icon.asSocial(FontAwesomeIcons.google,
                              Theme.Colors.colorGoogle)),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  void toggleSignInPassword() {
    setState(() {
      obscureTextSignInPassword = !obscureTextSignInPassword;
    });
  }

  @override
  void _signIn() {
    final form = _key.currentState;

    if (form.validate()) {
      form.save();
      if (_eMail != "" && _password != "") {
        presenter.onSignIn(_eMail, _password);
      }
    }
  }

  @override
  void onSignInResponse() {
    Navigator.of(_parentContext).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => SettingsPage(false), fullscreenDialog: false),
        (Route<dynamic> route) => false);
  }

  @override
  void onSignInFailure(ErrorResponse error) {
    UIHelper.showSnackBarByContext(_parentContext, error.text.toString(),
        duration: UIHelper.SHORT_TIME);
  }

  @override
  void onSocialSignInFailure(String errorText) {
    UIHelper.showSnackBarByContext(_parentContext, errorText,
        duration: UIHelper.SHORT_TIME);
  }
}
