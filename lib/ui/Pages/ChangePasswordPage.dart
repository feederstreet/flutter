import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/ChangeSettingsRequest.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/utils/ValidateHelper.dart';

class ChangePasswordPage extends StatefulWidget {
  ChangePasswordPageState createState() {
    return new ChangePasswordPageState();
  }
}

class ChangePasswordPageState extends State<ChangePasswordPage>
    with SingleTickerProviderStateMixin {
  final _key = new GlobalKey<FormState>();

  var _paswordController = new TextEditingController();
  var _paswordConfirmController = new TextEditingController();

  bool obscureTextPassword = true;
  bool obscureTextConfirmPassword = true;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "ŞİFRE DEĞİŞTİR",
          style: Theme.Text.titleStyle(size: 18.0),
        ),
        centerTitle: true,
      ),
      body: Builder(
        builder: (context) =>
            NotificationListener<OverscrollIndicatorNotification>(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: Theme.Background.decoration,
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Form(
                          key: _key,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: TextFormField(
                                  controller: _paswordController,
                                  obscureText: obscureTextPassword,
                                  validator: (val) =>
                                      ValidateHelper.password(val.trim()),
                                  onSaved: (val) =>
                                      _paswordController.text = val,
                                  style: Theme.TextField.textStyle,
                                  decoration: InputDecoration(
                                    icon: Theme.Icon.instance(
                                        FontAwesomeIcons.lock),
                                    border: InputBorder.none,
                                    labelText: "Şifre",
                                    labelStyle: Theme.TextField.labelStyle,
                                    errorStyle: Theme.TextField.errorStyle,
                                    suffixIcon: GestureDetector(
                                      onTap: () => {
                                            setState(() {
                                              obscureTextPassword =
                                                  !obscureTextPassword;
                                            })
                                          },
                                      child: Theme.TextField.passwordIcon,
                                    ),
                                  ),
                                ),
                              ),
                              Theme.Line.both(double.infinity),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: TextFormField(
                                  obscureText: obscureTextConfirmPassword,
                                  validator: (val) => confirmPassword(val),
                                  onSaved: (val) =>
                                      _paswordConfirmController.text = val,
                                  style: Theme.TextField.textStyle,
                                  decoration: InputDecoration(
                                    icon: Theme.Icon.instance(
                                        FontAwesomeIcons.lock),
                                    border: InputBorder.none,
                                    labelText: "Şifre Tekrar",
                                    labelStyle: Theme.TextField.labelStyle,
                                    errorStyle: Theme.TextField.errorStyle,
                                    suffixIcon: GestureDetector(
                                      onTap: () => {
                                            setState(() {
                                              obscureTextConfirmPassword =
                                                  !obscureTextConfirmPassword;
                                            })
                                          },
                                      child: Theme.TextField.passwordIcon,
                                    ),
                                  ),
                                ),
                              ),
                              Theme.Line.both(double.infinity),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
      ),
      bottomNavigationBar: Container(
        decoration: new BoxDecoration(
          color: Theme.Button.backgroundColor,
        ),
        child: MaterialButton(
          elevation: Theme.Button.elevation,
          minWidth: double.infinity,
          onPressed: () => _saved(context),
          child: Container(
            padding: EdgeInsets.all(14.0),
            child: Text(
              "Kaydet",
              style: Theme.Button.textStyle,
            ),
          ),
        ),
      ),
    );
  }

  Future _saved(BuildContext context) async {
    final form = _key.currentState;

    if (form.validate()) {
      form.save();

      var user = await SharedPreferencesHelper.getUser();
      ChangeSettingsRequest request = new ChangeSettingsRequest();
      if (this._paswordConfirmController.text != user.password)
        request.password = this._paswordConfirmController.text;

      if (request != null) {
        GenericResponse response = await Api.changeSettings(
            await SharedPreferencesHelper.getToken(), request.toJson());

        if (response.success) {
          return await onResponse(
              (await Api.getProfile(await SharedPreferencesHelper.getToken()))
                  .user);
        } else {
          return onFailure(response.error);
        }
      }
    }
  }

  Future refreshData() async {
    User user = await SharedPreferencesHelper.getUser();
    setState(() {
      if (user.password != null) {
        this._paswordController.text = user.password;
        this._paswordConfirmController.text = user.password;
      }

      final form = _key.currentState;
      form.save();
    });
  }

  String confirmPassword(String val) {
    final form = _key.currentState;
    form.save();

    if (val != _paswordController.text) {
      return "Şifreler aynı olmalı";
    }

    return ValidateHelper.password(val);
  }

  void onFailure(ErrorResponse error) {
    UIHelper.showSnackBarByKey(_scaffoldKey, error.text,
        duration: UIHelper.SHORT_TIME);
  }

  Future onResponse(User user) async {
    Navigator.of(context).pop(true);
  }
}
