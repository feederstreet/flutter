import 'package:flutter/material.dart';
import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/presenters/LobbyPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/ChatPage.dart';
import 'package:gamerhub/ui/Pages/OtherUserPage.dart';
import 'package:gamerhub/ui/Pages/ProfilPage.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/FormatHelper.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/views/LobbyView.dart';
import 'package:percent_indicator/percent_indicator.dart';

class LobbyPage extends StatefulWidget {
  Lobby _lobby;

  LobbyPage(this._lobby);

  @override
  LobbyPageState createState() {
    return new LobbyPageState(this._lobby);
  }
}

class LobbyPageState extends State<LobbyPage> implements LobbyView {
  Lobby _lobby;
  String _time = "";
  bool _isMyLoby = false;
  bool _inMyLoby = false;

  LobbyPresenter presenter;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  LobbyPageState(_lobby) {
    presenter = new LobbyPresenter(this);
    SharedPreferencesHelper.getUser().then((user) {
      presenter.refreshLobby(_lobby).then((val) {
        _time = DateFormatter.dateByMilForLobby(val.createdAt);
        print("user ");
        setState(() {
          this._lobby = val;
          _inMyLoby = this._lobby.id == user.lobbyId ? true : false;
          _isMyLoby = this._lobby.owner.id == user.id ? true : false;
        });
      });
    });
  }

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    Widget view = new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.Background.backgroundColor,
      appBar: new AppBar(
        title: new Text(
          _lobby != null ? _lobby.game.shortName : "LOBİ",
          style: new TextStyle(color: Theme.Text.titleStyle().color),
        ),
        centerTitle: _lobby != null ? false : true,
      ),
      body: new Container(
        child: new Column(
          children: <Widget>[
            _lobby != null
                ? new Container(
                    color: const Color(0xFF24242C),
                    child: new Container(
                      padding: EdgeInsets.all(16),
                      child: new Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new CircularPercentIndicator(
                                    radius: 100.0,
                                    percent: (_lobby.lobbyCount.toDouble() /
                                        _lobby.memberLimit.toDouble()),
                                    center: new Text(
                                      ((_lobby.lobbyCount.toDouble() /
                                                      _lobby.memberLimit
                                                          .toDouble()) *
                                                  100)
                                              .toInt()
                                              .toString() +
                                          "%",
                                      style: new TextStyle(
                                          color:
                                              Theme.Background.backgroundColor,
                                          fontSize: 25,
                                          fontWeight: Theme.Text.headerStyle()
                                              .fontWeight,
                                          fontFamily: Theme.Text.headerStyle()
                                              .fontFamily),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      _lobby.lobbyCount.toString() +
                                          "/" +
                                          _lobby.memberLimit.toString(),
                                      style: new TextStyle(
                                          color: Theme
                                              .Background.backgroundColor
                                              .withOpacity(0.5),
                                          fontFamily: Theme.Text.headerStyle()
                                              .fontFamily),
                                    ),
                                  )
                                ],
                              ),
                              new Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /*new Text(
                                    "Bu lobi " + _time + "'dır açık",
                                    style: new TextStyle(
                                        color: Theme.Text.titleStyle()
                                            .color
                                            .withOpacity(0.7)),
                                  ),*/
                                  new Container(
                                    width: 210,
                                    child: new Text(
                                      _lobby.game.name,
                                      style: new TextStyle(
                                          color: Theme
                                              .Background.backgroundColor
                                              .withOpacity(0.9),
                                          fontSize:
                                              Theme.Text.headerStyle().fontSize,
                                          fontWeight: Theme.Text.headerStyle()
                                              .fontWeight,
                                          fontFamily: Theme.Text.headerStyle()
                                              .fontFamily),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: new Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.only(
                                              left: 4,
                                              top: 4,
                                              bottom: 4,
                                              right: 12),
                                          width: 30.0,
                                          height: 30.0,
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            image: DecorationImage(
                                                image: _lobby.game.rank.avatar
                                                        .isNotEmpty
                                                    ? NetworkImage(Constants
                                                            .API_BASE_URL +
                                                        _lobby.game.rank.avatar)
                                                    : Theme.PlaceHolder.object,
                                                fit: BoxFit.cover),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(75.0)),
                                            boxShadow: [
                                              BoxShadow(
                                                  spreadRadius: 3.0,
                                                  color: Theme
                                                      .Colors.primarySoft
                                                      .withOpacity(0.7))
                                            ],
                                          ),
                                        ),
                                        new Text(
                                          _lobby.game.rank.name,
                                          style: new TextStyle(
                                              color: Theme
                                                  .Background.backgroundColor
                                                  .withOpacity(0.6),
                                              fontSize: 18,
                                              fontFamily:
                                                  Theme.Text.headerStyle()
                                                      .fontFamily),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                : new Container(),
            _lobby != null
                ? new Container(
                    padding: EdgeInsets.all(16),
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        !_inMyLoby
                            ? new Container(
                                child: Container(
                                  decoration: Theme.Button.boxDecoration,
                                  child: MaterialButton(
                                    elevation: Theme.Button.elevation,
                                    minWidth: double.infinity,
                                    onPressed: () =>
                                        presenter.joinLobby(_lobby),
                                    child: Container(
                                      padding: EdgeInsets.all(14.0),
                                      child: Text(
                                        "Lobiye Katıl",
                                        style: Theme.Button.textStyle,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : new Container(
                                child: Container(
                                  decoration: Theme.Button.boxDecoration,
                                  child: MaterialButton(
                                    elevation: Theme.Button.elevation,
                                    minWidth: double.infinity,
                                    onPressed: () =>
                                        presenter.exitLobby(_lobby),
                                    child: Container(
                                      padding: EdgeInsets.all(14.0),
                                      child: Text(
                                        !_isMyLoby
                                            ? "Lobiden Ayrıl"
                                            : "Lobiyi Kapat",
                                        style: Theme.Button.textStyle,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                        _lobby.owner != null
                            ? new Container(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.only(top: 16),
                                      child: new Text(
                                        "Lobi Kurucusu",
                                        style: Theme.Text.headerStyle(),
                                      ),
                                    ),
                                    GestureDetector(
                                      child: new Card(
                                        shape: Theme.Card.shape,
                                        margin: EdgeInsets.only(top: 8),
                                        child: new Container(
                                          child: new Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 16.0,
                                                horizontal: 16.0),
                                            child: new Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: new Text(
                                                    _lobby.owner.username,
                                                    style:
                                                        Theme.Text.textStyle(),
                                                  ),
                                                ),
                                                Theme.Icon.instance(
                                                    Icons.chevron_right),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      onTap: () => goToFriend(_lobby.owner),
                                    )
                                  ],
                                ),
                              )
                            : new Container(),
                        _lobby.members != null
                            ? _lobby.members.length > 0
                                ? new Container(
                                    margin: EdgeInsets.only(top: 16),
                                    child: new Text(
                                      "Üyeler",
                                      style: Theme.Text.headerStyle(),
                                    ),
                                  )
                                : new Container()
                            : new Container(),
                        _lobby.members != null
                            ? new Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount: _lobby.members.length,
                                    itemBuilder: (context, position) {
                                      return GestureDetector(
                                        child: new Card(
                                          margin: EdgeInsets.only(top: 8),
                                          shape: Theme.Card.shape,
                                          child: new Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 16.0,
                                                horizontal: 16.0),
                                            child: new Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: new Text(
                                                    _lobby.members[position]
                                                        .username,
                                                    style:
                                                        Theme.Text.textStyle(),
                                                  ),
                                                ),
                                                Theme.Icon.instance(
                                                    Icons.chevron_right),
                                              ],
                                            ),
                                          ),
                                        ),
                                        onTap: () => goToFriend(
                                            _lobby.members[position]),
                                      );
                                    }),
                              )
                            : new Container(),
                      ],
                    ),
                  )
                : new Container(),
          ],
        ),
      ),
    );
    return view;
  }

  void goToMessagePage(User user) {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new ChatPage(user)));
  }

  Future goToFriend(User target) async {
    SharedPreferencesHelper.getUser().then((user) {
      if (_lobby.owner.email != user.email) {
        print(_lobby.owner.id);
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new OtherUserPage(target.id)));
      } else
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new ProfilPage(context)));
    });
  }

  @override
  void onFailure(ErrorResponse error) {
    UIHelper.showSnackBarByKey(_scaffoldKey, error.text,
        duration: UIHelper.SHORT_TIME);
  }

  void join() {
    SharedPreferencesHelper.getUser().then((user) {
      presenter.refreshLobby(_lobby).then((val) {
        setState(() {
          _lobby = val;
          _inMyLoby = this._lobby.id == user.lobbyId ? true : false;
          _isMyLoby = this._lobby.owner.id == user.id ? true : false;
        });
      });
    });
  }

  void exit() {
    presenter.refreshLobby(_lobby).then((val) {
      setState(() {
        _lobby = val;
        _isMyLoby = false;
        _inMyLoby = false;
      });
    });

    Navigator.of(context).pop(true);
  }
}
