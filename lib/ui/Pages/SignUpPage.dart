import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/presenters/SignUpPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/SettingsPage.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/utils/ValidateHelper.dart';
import 'package:gamerhub/views/SignUpView.dart';

class SignUpPage extends StatefulWidget {
  BuildContext _parentContext;

  SignUpPage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new SignUpPageState(_parentContext);
  }
}

class SignUpPageState extends State<SignUpPage>
    with TickerProviderStateMixin
    implements SignUpView {
  SignUpPresenter presenter;
  final BuildContext _parentContext;
  final key = new GlobalKey<FormState>();

  SignUpPageState(this._parentContext) {
    presenter = new SignUpPresenter(this);
  }

  String _userName;
  String _eMail;
  String _password;
  String _confirmPassword;

  bool obscureTextSignupPassword = true;
  bool obscureTextSignupConfirmPassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 20.0, right: 40.0, left: 40.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              overflow: Overflow.visible,
              children: <Widget>[
                Card(
                  elevation: Theme.Card.elevation,
                  color: Theme.Card.backgroundColor,
                  shape: Theme.Card.shape,
                  child: Container(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Form(
                        key: key,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                validator: (val) =>
                                    ValidateHelper.userName(val),
                                onSaved: (val) => _userName = val,
                                style: Theme.TextField.textStyle,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Theme.Icon.instance(
                                    FontAwesomeIcons.user,
                                  ),
                                  labelText: "Kullanı Adı",
                                  labelStyle: Theme.TextField.labelStyle,
                                  errorStyle: Theme.TextField.errorStyle,
                                ),
                              ),
                            ),
                            Theme.Line.both(double.infinity),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                style: Theme.TextField.textStyle,
                                validator: (val) => ValidateHelper.eMail(val),
                                onSaved: (val) => _eMail = val,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Theme.Icon.instance(
                                    FontAwesomeIcons.envelope,
                                  ),
                                  labelText: "E Posta",
                                  labelStyle: Theme.TextField.labelStyle,
                                  errorStyle: Theme.TextField.errorStyle,
                                ),
                              ),
                            ),
                            Theme.Line.both(double.infinity),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: TextFormField(
                                obscureText: obscureTextSignupPassword,
                                validator: (val) =>
                                    ValidateHelper.password(val.trim()),
                                onSaved: (val) => _password = val,
                                style: Theme.TextField.textStyle,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Theme.Icon.instance(
                                    FontAwesomeIcons.lock,
                                  ),
                                  labelText: "Şifre",
                                  labelStyle: Theme.TextField.labelStyle,
                                  errorStyle: Theme.TextField.errorStyle,
                                  suffixIcon: GestureDetector(
                                    onTap: toggleSignup,
                                    child: Theme.TextField.passwordIcon,
                                  ),
                                ),
                              ),
                            ),
                            Theme.Line.both(double.infinity),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: TextFormField(
                                obscureText: obscureTextSignupConfirmPassword,
                                validator: (val) => confirmPassword(val),
                                onSaved: (val) => _confirmPassword = val,
                                style: Theme.TextField.textStyle,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Theme.Icon.instance(
                                      FontAwesomeIcons.lock),
                                  labelText: "Şifre Tekrar",
                                  labelStyle: Theme.TextField.labelStyle,
                                  errorStyle: Theme.TextField.errorStyle,
                                  suffixIcon: GestureDetector(
                                    onTap: toggleSignupConfirm,
                                    child: Theme.TextField.passwordIcon,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              decoration: Theme.Button.boxDecoration,
                              child: MaterialButton(
                                elevation: Theme.Button.elevation,
                                minWidth: double.infinity,
                                onPressed: () => _signUp(),
                                child: Container(
                                  padding: EdgeInsets.all(14.0),
                                  child: Text(
                                    "Kayıt Ol",
                                    style: Theme.Button.textStyle,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  void toggleSignup() {
    setState(() {
      obscureTextSignupPassword = !obscureTextSignupPassword;
    });
  }

  void toggleSignupConfirm() {
    setState(() {
      obscureTextSignupConfirmPassword = !obscureTextSignupConfirmPassword;
    });
  }

  @override
  String confirmPassword(String val) {
    final form = key.currentState;
    form.save();

    if (val != _password) {
      return "Şifreler aynı olmalı";
    }

    return ValidateHelper.password(val);
  }

  @override
  void _signUp() {
    final form = key.currentState;

    if (form.validate()) {
      form.save();
      if (_eMail != "" && _password != "") {
        presenter.signUp(_userName, _eMail, _password);
      }
    }
  }

  @override
  void onFailure(ErrorResponse error) {
    UIHelper.showSnackBarByContext(_parentContext, error.text,
        duration: UIHelper.SHORT_TIME);
  }

  @override
  void onResponse() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => SettingsPage(false), fullscreenDialog: true),
        (Route<dynamic> route) => false);
  }
}
