import 'dart:async';

import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/presenters/AddLobbyPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/ListViews/GamesListView.dart';
import 'package:gamerhub/ui/Loader.dart';
import 'package:gamerhub/ui/Pages/AddLobbyDetailPage.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/views/SelectGameForLobbyView.dart';

class SelectGameForLobbyPage extends StatefulWidget {
  BuildContext _parentContext;

  SelectGameForLobbyPage(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new SelectGameForLobbyPageState(_parentContext);
  }
}

class SelectGameForLobbyPageState extends State<SelectGameForLobbyPage>
    implements SelectGameForLobbyView {
  AddLobbyPresenter _presenter;
  BuildContext _parentContext;

  List<Game> games = new List<Game>();
  Game _selectGame;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _asyncLoaderState = new GlobalKey<AsyncLoaderState>();

  SelectGameForLobbyPageState(this._parentContext) {
    _presenter = new AddLobbyPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: new Text(
          "OYUN SEÇ",
        ),
        centerTitle: true,
      ),
      body: new Container(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new AsyncLoader(
              key: _asyncLoaderState,
              initState: () => refreshGames(),
              renderLoad: () => Expanded(
                    child: Center(
                      child: new Loader(
                        dotOneColor: Colors.blue,
                        dotTwoColor: Colors.red,
                        dotThreeColor: Colors.yellow,
                        dotType: DotType.circle,
                      ),
                    ),
                  ),
              renderError: ([error]) => new Expanded(
                    child: new Center(
                      child: Container(
                        margin: EdgeInsets.only(top: 16.0),
                        decoration: Theme.Button.boxDecoration,
                        child: MaterialButton(
                          elevation: Theme.Button.elevation,
                          onPressed: () =>
                              _asyncLoaderState.currentState.reloadState(),
                          child: Container(
                            padding: EdgeInsets.all(14.0),
                            child: Text(
                              "Tekrar Dene",
                              style: Theme.Button.textStyle,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
              renderSuccess: ({data}) => this.games.length > 0
                  ? new Expanded(
                      child: new GamesListView(_parentContext, this, data),
                    )
                  : new Expanded(
                      child: new Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Oyun Bulunmamaktadır!",
                              style: Theme.Text.titleStyle(),
                            ),
                          ],
                        ),
                      ),
                    ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        decoration: new BoxDecoration(
          color: Theme.Button.backgroundColor,
        ),
        child: MaterialButton(
          elevation: Theme.Button.elevation,
          minWidth: double.infinity,
          onPressed: () => goToAddLobby(),
          child: Container(
            padding: EdgeInsets.all(14.0),
            child: Text(
              "SEÇ",
              style: Theme.Button.textStyle,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Future<List<Game>> refreshGames() async {
    this.games.clear();
    List<Game> games = await _presenter.getGames();
    this.games = games;
    await Future.delayed(Duration(seconds: 1));
    return games;
  }

  void goToAddLobby() {
    if (_selectGame != null) {
      Navigator.of(_parentContext).push(new MaterialPageRoute(
          builder: (BuildContext context) =>
              new AddLobbyDetailPage(_parentContext, _selectGame)));
    } else {
      UIHelper.showSnackBarByKey(_scaffoldKey, "Oyun Seçiniz");
    }
  }

  void selectGame(Game game) {
    _selectGame = game;
  }
}
