import 'package:flutter/material.dart';
import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/model/Rank.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/lobby/CreateLobbyRequest.dart';
import 'package:gamerhub/network/response/LobbyResponse.dart';
import 'package:gamerhub/presenters/AddLobbyDetailPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/HomePage.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/utils/UIHelper.dart';
import 'package:gamerhub/views/AddLobbyDetailView.dart';

class AddLobbyDetailPage extends StatefulWidget {
  BuildContext _parentContext;
  Game _game;

  AddLobbyDetailPage(this._parentContext, this._game);

  @override
  State<StatefulWidget> createState() {
    return new AddLobbyPageDetailState(this._parentContext, this._game);
  }
}

class AddLobbyPageDetailState extends State<AddLobbyDetailPage>
    implements AddLobbyDetailView {
  AddLobbyDetailPresenter _presenter;
  BuildContext _parentContext;
  Game _game;
  Rank _selectRank;
  int _selectRankPosition;
  var _memberLimitController = new TextEditingController();
  int _memberLimit = 0;
  int _stepIndex = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  AddLobbyPageDetailState(this._parentContext, this._game);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: new Text(
          "LOBİ EKLE",
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: Theme.Background.decoration,
        child: Stepper(
          type: StepperType.horizontal,
          steps: [
            Step(
              title: Text(""),
              isActive: _stepIndex == 0 ? true : false,
              content: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(bottom: 8),
                    child: new Text("Rank Seçiniz",
                        style: Theme.Text.titleStyle()),
                  ),
                  ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: _game.ranks.length,
                      shrinkWrap: true,
                      itemBuilder: (context, position) {
                        return GestureDetector(
                          child: new Card(
                            elevation: Theme.Card.elevation,
                            color: _selectRankPosition == position
                                ? Colors.lightGreenAccent.shade100
                                : Theme.Card.backgroundColor,
                            shape: Theme.Card.shape,
                            child: new Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 15.0, horizontal: 15.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 16),
                                    width: 60.0,
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        image: DecorationImage(
                                            image:
                                                _game.ranks[position].avatar !=
                                                        null
                                                    ? NetworkImage(
                                                        Constants.API_BASE_URL +
                                                            this
                                                                ._game
                                                                .ranks[position]
                                                                .avatar)
                                                    : Theme.PlaceHolder.object,
                                            fit: BoxFit.cover),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(75.0)),
                                        boxShadow: [
                                          BoxShadow(
                                              blurRadius: 2.0,
                                              color: Colors.black)
                                        ]),
                                  ),
                                  new Text(
                                    _game.ranks[position].name,
                                    style: Theme.Text.headerStyle(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          onTap: () => setState(() {
                                this._selectRank = _game.ranks[position];
                                this._selectRankPosition = position;
                              }),
                        );
                      }),
                ],
              ),
            ),
            Step(
              isActive: _stepIndex == 1 ? true : false,
              title: Text(""),
              content: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(bottom: 8),
                    child: new Text("Üye Sayısı Giriniz",
                        style: Theme.Text.titleStyle()),
                  ),
                  new TextFormField(
                    controller: _memberLimitController,
                    keyboardType: TextInputType.number,
                    autofocus: false,
                    onSaved: (String val) => _memberLimitController.text = val,
                    textCapitalization: TextCapitalization.words,
                    style: Theme.TextField.textStyle,
                  ),
                ],
              ),
            ),
          ],
          currentStep: _stepIndex,
          controlsBuilder: (BuildContext context,
              {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
            return new Container();
          },
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.transparent,
        height: 64,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: MaterialButton(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Icon(
                      Icons.chevron_left,
                      color: _stepIndex > 0
                          ? Theme.Button.backgroundColor
                          : Theme.Icon.iconColor,
                      size: 48,
                    ),
                    new Text(
                      "GERİ",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: _stepIndex > 0
                            ? Theme.Button.backgroundColor
                            : Theme.Icon.iconColor,
                      ),
                    ),
                  ],
                ),
                onPressed: () {
                  if (_stepIndex != 0) {
                    setState(() {
                      _stepIndex--;
                    });
                  }
                },
              ),
            ),
            Align(
                alignment: Alignment.centerRight,
                child: MaterialButton(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        (_stepIndex < 1) ? "İLERİ" : "LOBİ AÇ",
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Theme.Button.backgroundColor,
                        ),
                      ),
                      new Icon(
                        Icons.chevron_right,
                        color: Theme.Button.backgroundColor,
                        size: 48,
                      ),
                    ],
                  ),
                  onPressed: () {
                    setState(() {
                      _memberLimit =
                          getMemberText(this._memberLimitController.text);
                    });

                    if (_stepIndex != 1 && _selectRank != null) {
                      setState(() {
                        _stepIndex++;
                      });
                    } else if (_selectRank == null) {
                      UIHelper.showSnackBarByKey(_scaffoldKey, "Rank Seçiniz",
                          textColor: Theme.Text.headerStyle().color,
                          backGroundColor: Theme.Background.backgroundColor);
                    } else if (_memberLimit < 2) {
                      UIHelper.showSnackBarByKey(
                          _scaffoldKey, "Üye sayısı en az 2 kişi olabilir",
                          textColor: Theme.Text.headerStyle().color,
                          backGroundColor: Theme.Background.backgroundColor);
                    } else if (_stepIndex == 1 &&
                        _selectRank != null &&
                        _memberLimit >= 2) {
                      createLobby();
                    }
                  },
                )),
          ],
        ),
      ),
    );
  }

  int getMemberText(String text) {
    int sayi = 0;

    try {
      sayi = int.parse(text);
    } catch (e) {
      sayi = 0;
    }
    return sayi;
  }

  void createLobby() async {
    CreateLobbyRequest request = new CreateLobbyRequest();
    request.gameId = _game.sId;
    request.memberLimit = _memberLimit;
    request.rankName = _selectRank.name;

    LobbyResponse response = await Api.createLobby(
        await SharedPreferencesHelper.getToken(), request.toJson());

    if (response.lobby != null) {
      goToHome();
    } else {
      UIHelper.showSnackBarByKey(
          _scaffoldKey, response.genericResponse.error.text,
          textColor: Theme.Text.headerStyle().color,
          backGroundColor: Theme.Background.backgroundColor);
    }
  }

  void goToHome() async {
    Navigator.pushAndRemoveUntil(
        _parentContext,
        MaterialPageRoute(
            builder: (context) => HomePage(), fullscreenDialog: false),
        (Route<dynamic> route) => false);
  }
}
