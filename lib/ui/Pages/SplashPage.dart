import 'package:flutter/material.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/HomePage.dart';
import 'package:gamerhub/ui/Pages/SettingsPage.dart';
import 'package:gamerhub/ui/Pages/SignPage.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class SplashPage extends StatefulWidget {
  SplashPageState _state;

  SplashPage() {
    _state = new SplashPageState();
  }

  SplashPageState createState() => _state;
}

class SplashPageState extends State<SplashPage> {
  AnimationController animationController;
  Animation<double> animation;
  var isProgressVisible = true;
  var isLogoVisible = true;

  @override
  void initState() {
    super.initState();

    SharedPreferencesHelper.getToken().then((val) async {
      if (val == null || val == "" || val.isEmpty) {
        setState(() {
          isProgressVisible = false;
        });

        Future.delayed(Duration(milliseconds: 1450), () {
          setState(() {
            isLogoVisible = false;
          });
        });

        Future.delayed(Duration(milliseconds: 2000), () {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => SignPage(), fullscreenDialog: false),
              (Route<dynamic> route) => false);
        });
      } else {
        await Api.getProfile(val);
        bool settingsVal = (await SharedPreferencesHelper.getSettingsVal());

        setState(() {
          isProgressVisible = false;
        });

        Future.delayed(Duration(milliseconds: 1450), () {
          setState(() {
            isLogoVisible = false;
          });
        });
        Future.delayed(Duration(milliseconds: 2000), () {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      settingsVal ? HomePage() : SettingsPage(false),
                  fullscreenDialog: false),
              (Route<dynamic> route) => false);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Builder(builder: (context) {
      return new Container(
        decoration: Theme.Background.decoration,
        height: double.maxFinite,
        width: double.maxFinite,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: new Container(),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    AnimatedOpacity(
                      opacity: isLogoVisible ? 1 : 0,
                      duration: new Duration(milliseconds: 500),
                      child: Image.asset(
                        "assets/icon/isimli.png",
                        width: 200,
                        height: 200,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: new Container(
                child: new Center(
                  child: AnimatedOpacity(
                    opacity: isProgressVisible ? 1 : 0,
                    duration: new Duration(milliseconds: 500),
                    child: new CircularProgressIndicator(),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }));
  }
}
