import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';
import 'package:gamerhub/network/response/ProfileResponse.dart';
import 'package:gamerhub/presenters/OtherUserPresenter.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/ChatPage.dart';
import 'package:gamerhub/ui/Pages/LobbyPage.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/utils/FormatHelper.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/OtherUserView.dart';

class OtherUserPage extends StatefulWidget {
  String _userId = "";

  OtherUserPage(_userId) {
    this._userId = _userId;
  }

  @override
  State<StatefulWidget> createState() {
    return new OtherUserPageState(_userId);
  }
}

class OtherUserPageState extends State<OtherUserPage> implements OtherUserView {
  OtherUserPresenter presenter;

  final _key = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String _userId = "";

  String photoUrl = "";
  String fullName = "";
  String eMail = "";
  String gender = "";
  String userName = "";
  String birthDate = "";
  String age = "";

  String facebookUrl = "";
  String discordUrl = "";

  Lobby lobby;

  bool isFriend = false;
  bool hasFriendRequest = false;

  OtherUserPageState(String _userId) {
    this._userId = _userId;
    presenter = new OtherUserPresenter(this);
  }

  @override
  void initState() {
    presenter.initView(_userId);
  }

  @override
  Widget build(BuildContext context) {
    Widget view = new Scaffold(
        backgroundColor: Theme.Background.backgroundColor,
        key: _scaffoldKey,
        appBar: AppBar(
          title: new Text(
            userName.isNotEmpty ? userName : "",
          ),
          centerTitle: userName.isNotEmpty ? false : true,
        ),
        body: Builder(
          builder: (context) =>
              NotificationListener<OverscrollIndicatorNotification>(
                child: Container(
                  decoration: Theme.Background.decoration,
                  child: SafeArea(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: 16, left: 16.0, right: 16.0, bottom: 16.0),
                            child: Form(
                              key: _key,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  SizedBox(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(right: 16),
                                          width: 100.0,
                                          height: 100.0,
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            image: DecorationImage(
                                                image: this.photoUrl.isNotEmpty
                                                    ? NetworkImage(
                                                        this.photoUrl)
                                                    : Theme.PlaceHolder.user,
                                                fit: BoxFit.contain),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(75.0)),
                                            boxShadow: [
                                              BoxShadow(
                                                  blurRadius: 5.0,
                                                  color: Colors.black)
                                            ],
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: fullName.isNotEmpty
                                                  ? new Text(
                                                      fullName,
                                                      style: Theme.Text
                                                          .headerStyle(
                                                              size: 18.0),
                                                    )
                                                  : new Container(),
                                            ),
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  eMail.isNotEmpty
                                                      ? new Text(eMail,
                                                          style: Theme.Text
                                                              .textStyle())
                                                      : new Container(),
                                                  gender.isNotEmpty
                                                      ? new Text(gender,
                                                          style: Theme.Text
                                                              .textStyle())
                                                      : new Container(),
                                                  birthDate.isNotEmpty
                                                      ? new Text(
                                                          birthDate +
                                                              ((age.isNotEmpty)
                                                                  ? " (" +
                                                                      age +
                                                                      " Yaşında)"
                                                                  : ""),
                                                          style: Theme.Text
                                                              .textStyle())
                                                      : new Container(),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  isFriend
                                      ? new Container(
                                          margin: EdgeInsets.only(top: 16.0),
                                          child: new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.all(5),
                                                  decoration: Theme
                                                      .Button.boxDecoration,
                                                  child: MaterialButton(
                                                    elevation:
                                                        Theme.Button.elevation,
                                                    onPressed: () => presenter
                                                        .removeFriend(_userId),
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(14.0),
                                                      child: Text(
                                                        "Takibi Bırak",
                                                        style: Theme
                                                            .Button.textStyle,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.all(5),
                                                  decoration: Theme
                                                      .Button.boxDecoration,
                                                  child: MaterialButton(
                                                    elevation:
                                                        Theme.Button.elevation,
                                                    onPressed: () =>
                                                        gotToMessagePage(),
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(14.0),
                                                      child: Text(
                                                        "Mesaj At",
                                                        style: Theme
                                                            .Button.textStyle,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      : (this.hasFriendRequest
                                          ? new Container(
                                              margin:
                                                  EdgeInsets.only(top: 16.0),
                                              child: Container(
                                                decoration:
                                                    Theme.Button.boxDecoration,
                                                child: MaterialButton(
                                                  elevation:
                                                      Theme.Button.elevation,
                                                  minWidth: double.infinity,
                                                  onPressed: () => presenter
                                                      .cancelRequest(_userId),
                                                  child: Container(
                                                    padding:
                                                        EdgeInsets.all(14.0),
                                                    child: Text(
                                                      "İstek Gönderildi",
                                                      style: Theme
                                                          .Button.textStyle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : new Container(
                                              margin:
                                                  EdgeInsets.only(top: 16.0),
                                              child: Container(
                                                decoration:
                                                    Theme.Button.boxDecoration,
                                                child: MaterialButton(
                                                  elevation:
                                                      Theme.Button.elevation,
                                                  minWidth: double.infinity,
                                                  onPressed: () => presenter
                                                      .addFriend(_userId),
                                                  child: Container(
                                                    padding:
                                                        EdgeInsets.all(14.0),
                                                    child: Text(
                                                      "Arkadaşlık İsteği Gönder",
                                                      style: Theme
                                                          .Button.textStyle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )),
                                  new Container(
                                      margin: EdgeInsets.only(top: 16),
                                      child: new Text("İletişim Linkleri",
                                          style: Theme.Text.titleStyle())),
                                  new Card(
                                    margin: EdgeInsets.only(top: 8.0),
                                    elevation: Theme.Card.elevation,
                                    shape: Theme.Card.shape,
                                    child: new Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 8.0, horizontal: 16.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            padding: EdgeInsets.only(
                                                right: 8.0,
                                                top: 16.0,
                                                bottom: 16.0),
                                            child: Icon(
                                              FontAwesomeIcons.discord,
                                              color: Theme.Colors.colorDiscord,
                                            ),
                                          ),
                                          new Text(
                                            discordUrl.isNotEmpty
                                                ? discordUrl
                                                : "...",
                                            style: Theme.Text.textStyle(
                                                size: 18.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  new Card(
                                    margin: EdgeInsets.only(top: 16.0),
                                    elevation: Theme.Card.elevation,
                                    shape: Theme.Card.shape,
                                    child: new Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 8.0, horizontal: 16.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            padding: EdgeInsets.only(
                                                right: 8.0,
                                                top: 16.0,
                                                bottom: 16.0),
                                            child: Icon(
                                                FontAwesomeIcons.facebookF,
                                                color:
                                                    Theme.Colors.colorFacebook),
                                          ),
                                          new Text(
                                            facebookUrl.isNotEmpty
                                                ? facebookUrl
                                                : "...",
                                            style: Theme.Text.textStyle(
                                                size: 18.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  lobby != null
                                      ? new Container(
                                          margin: EdgeInsets.only(top: 16),
                                          child: new Text("MEVCUT LOBİ",
                                              style: Theme.Text.titleStyle()),
                                        )
                                      : new Container(),
                                  lobby != null
                                      ? new Card(
                                          shape: Theme.Card.shape,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 15),
                                          child: new Container(
                                            decoration: new BoxDecoration(
                                              borderRadius:
                                                  Theme.Card.borderRadius,
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    Constants.API_BASE_URL +
                                                        lobby.game.avatar),
                                                colorFilter: ColorFilter
                                                    .srgbToLinearGamma(),
                                                fit: BoxFit.cover,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                    blurRadius: 3.0,
                                                    color: Colors.black)
                                              ],
                                            ),
                                            height: (133 +
                                                    (38 *
                                                        (lobby.game.rank != null
                                                            ? 1
                                                            : 0)))
                                                .toDouble(),
                                            child: GestureDetector(
                                              child: new Container(
                                                padding: EdgeInsets.only(
                                                    top: 16,
                                                    right: 16,
                                                    left: 16,
                                                    bottom: 10),
                                                decoration: new BoxDecoration(
                                                    borderRadius:
                                                        Theme.Card.borderRadius,
                                                    color: const Color(
                                                        0x44000000)),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .stretch,
                                                  children: <Widget>[
                                                    new Text(
                                                      lobby.game.name,
                                                      softWrap: true,
                                                      style: new TextStyle(
                                                          color: Theme.Text
                                                              .cardTextColor,
                                                          fontSize: 20.0,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    new Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                vertical: 8),
                                                        child: new Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            new Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              16),
                                                                  width: 60.0,
                                                                  height: 60.0,
                                                                  decoration: BoxDecoration(
                                                                      color: Colors
                                                                          .grey,
                                                                      image: DecorationImage(image: lobby.owner.avatar != null ? NetworkImage(Constants.API_BASE_URL + lobby.game.avatar) : Theme.PlaceHolder.user, fit: BoxFit.fill),
                                                                      borderRadius: BorderRadius.all(Radius.circular(60.0)),
                                                                      boxShadow: [
                                                                        BoxShadow(
                                                                            spreadRadius:
                                                                                3.0,
                                                                            color:
                                                                                Theme.Colors.primarySoft)
                                                                      ]),
                                                                ),
                                                                Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: <
                                                                      Widget>[
                                                                    new Text(
                                                                      lobby
                                                                          .owner
                                                                          .username,
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                      softWrap:
                                                                          true,
                                                                      style: new TextStyle(
                                                                          color: Theme
                                                                              .Text
                                                                              .cardTextColor,
                                                                          fontSize:
                                                                              20.0,
                                                                          fontWeight:
                                                                              FontWeight.bold),
                                                                    ),
                                                                    new Container(
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 2),
                                                                      child:
                                                                          new Text(
                                                                        (lobby.members.length + 1).toString() +
                                                                            " / " +
                                                                            lobby.memberLimit.toString(),
                                                                        style:
                                                                            new TextStyle(
                                                                          color: Theme
                                                                              .Text
                                                                              .cardTextColor,
                                                                          fontSize:
                                                                              18.0,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            new Container(
                                                              height: (38 *
                                                                      ((lobby.game.rank !=
                                                                              null
                                                                          ? 1
                                                                          : 0)))
                                                                  .toDouble(),
                                                              margin: EdgeInsets
                                                                  .only(top: 8),
                                                              child: ListView
                                                                  .builder(
                                                                scrollDirection:
                                                                    Axis.vertical,
                                                                itemCount: (lobby
                                                                            .game
                                                                            .rank !=
                                                                        null
                                                                    ? 1
                                                                    : 0),
                                                                itemBuilder:
                                                                    (context,
                                                                        rankPosition) {
                                                                  return new Row(
                                                                    mainAxisSize:
                                                                        MainAxisSize
                                                                            .min,
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: <
                                                                        Widget>[
                                                                      new Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                4,
                                                                            top:
                                                                                4,
                                                                            bottom:
                                                                                4,
                                                                            right:
                                                                                12),
                                                                        width:
                                                                            30.0,
                                                                        height:
                                                                            30.0,
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          color:
                                                                              Colors.grey,
                                                                          image: DecorationImage(
                                                                              image: lobby.game.rank.avatar.isNotEmpty ? NetworkImage(Constants.API_BASE_URL + lobby.game.rank.avatar) : Theme.PlaceHolder.object,
                                                                              fit: BoxFit.cover),
                                                                          borderRadius:
                                                                              BorderRadius.all(Radius.circular(75.0)),
                                                                          boxShadow: [
                                                                            BoxShadow(
                                                                                spreadRadius: 3.0,
                                                                                color: Theme.Colors.primarySoft.withOpacity(0.7))
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      new Text(
                                                                          lobby
                                                                              .game
                                                                              .rank
                                                                              .name,
                                                                          style: new TextStyle(
                                                                              color: Theme.Text.cardTextColor,
                                                                              fontSize: 18.0,
                                                                              fontWeight: FontWeight.bold))
                                                                    ],
                                                                  );
                                                                },
                                                              ),
                                                            )
                                                          ],
                                                        )),
                                                  ],
                                                ),
                                              ),
                                              onTap: () => _onTapLobi(),
                                            ),
                                          ),
                                        )
                                      : new Container(),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
        ));
    return view;
  }

  void _onTapLobi() {
    if (lobby != null)
      Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new LobbyPage(lobby),
      ));
  }

  @override
  void onFailure(ErrorResponse error) {}

  @override
  void onResponse(User user) {
    if (user != null) {
      refreshData(user);
    }
  }

  @override
  Future refreshData(User user) async {
    bool isFriendTemp = (await presenter.isFriend(_userId));
    bool hasFriendRequestTemp = (await presenter.hasFriendRequest(_userId));

    Lobby lobbyTemp = null;
    if (user.lobbyId != null) {
      if (user.lobbyId.isNotEmpty)
        lobbyTemp = await presenter.getLobby(user.lobbyId);
    }

    setState(() {
      if (user.avatar != null) {
        this.photoUrl = user.avatar;
      }
      this.fullName = user.fullname;
      this.gender = user.gender ? "Erkek" : "Kadın";
      if (user.birthDate != null) {
        this.birthDate = user.birthDate == null
            ? ""
            : DateFormatter.dateByMil(user.birthDate);
        int now =
            DateFormatter.getAsYear(new DateTime.now().millisecondsSinceEpoch);

        int t = DateFormatter.getAsYear(user.birthDate);
        this.age = ((now - t) > 0 ? (now - t) : "").toString();
      }
      if (user.facebookUrl != null) {
        this.facebookUrl = user.facebookUrl;
      }
      if (user.discordUrl != null) {
        this.discordUrl = user.discordUrl;
      }

      this.userName = user.username;
      this.eMail = user.email;

      this.isFriend = isFriendTemp;

      if (lobbyTemp != null)
        this.lobby = lobbyTemp;
      else
        this.lobby = null;

      this.hasFriendRequest = hasFriendRequestTemp;

      final form = _key.currentState;
      form.save();
    });
  }

  gotToMessagePage() async {
    if (_userId != null) {
      if (_userId.isNotEmpty) {
        ProfileResponse profileResponse = await Api.getFriendProfile(
            await SharedPreferencesHelper.getToken(), _userId);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new ChatPage(profileResponse.user),
        ));
      }
    }
  }
}
