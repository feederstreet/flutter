import 'package:flutter/material.dart';
import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/views/SelectGameForLobbyView.dart';

class GamesListView extends StatefulWidget {
  BuildContext _parentContext;
  SelectGameForLobbyView _addLobiView;
  List<Game> _games = new List();

  GamesListView(this._parentContext, this._addLobiView, this._games);

  @override
  State<StatefulWidget> createState() {
    return new GamesListViewState(
        this._parentContext, this._addLobiView, this._games);
  }
}

class GamesListViewState extends State<GamesListView> {
  BuildContext _parentContext;
  SelectGameForLobbyView _addLobiView;

  final List<Game> games;
  int select = -1;

  GamesListViewState(this._parentContext, this._addLobiView, this.games);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: games.length,
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
          itemBuilder: (context, position) {
            return GestureDetector(
              child: new Card(
                elevation: Theme.Card.elevation,
                color: select == position
                    ? Colors.lightGreenAccent.shade100
                    : Theme.Card.backgroundColor,
                shape: Theme.Card.shape,
                child: new Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 16),
                        width: 75.0,
                        height: 75.0,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            image: DecorationImage(
                                image: games[position].avatar != null
                                    ? NetworkImage(Constants.API_BASE_URL +
                                        this.games[position].avatar)
                                    : Theme.PlaceHolder.object,
                                fit: BoxFit.cover),
                            borderRadius:
                                BorderRadius.all(Radius.circular(75.0)),
                            boxShadow: [
                              BoxShadow(blurRadius: 3.0, color: Colors.black)
                            ]),
                      ),
                      Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                games[position].name,
                                style: Theme.Text.headerStyle(),
                              ),
                              new Text(
                                games[position].shortName,
                                style: Theme.Text.textStyle(),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
              onTap: () => _onTapItem(games[position], position),
            );
          }),
    );
  }

  void _onTapItem(Game game, int position) {
    setState(() {
      this.select = position;
    });

    if (_addLobiView != null) _addLobiView.selectGame(game);
  }
}
