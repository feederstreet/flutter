import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/FriendRequest.dart';
import 'package:gamerhub/network/response/FriendRequestsResponse.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Loader.dart';
import 'package:gamerhub/ui/Pages/OtherUserPage.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';

class FriendRequests extends StatefulWidget {
  BuildContext _parentContext;

  FriendRequests(this._parentContext);

  @override
  State<StatefulWidget> createState() {
    return new FriendRequestsListView(_parentContext);
  }
}

class FriendRequestsListView extends State<FriendRequests> {
  BuildContext _parentContext;

  List<User> _friendRequests = new List();
  var _asyncLoaderState = new GlobalKey<AsyncLoaderState>();

  FriendRequestsListView(this._parentContext);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: RefreshIndicator(
          child: Container(
            decoration: Theme.Background.decoration,
            child: new Column(
              children: <Widget>[
                new AsyncLoader(
                  key: _asyncLoaderState,
                  initState: () => refreshFriendRequests(),
                  renderLoad: () => Expanded(
                        child: Center(
                          child: new Loader(
                            dotOneColor: Colors.blue,
                            dotTwoColor: Colors.red,
                            dotThreeColor: Colors.yellow,
                            dotType: DotType.circle,
                          ),
                        ),
                      ),
                  renderError: ([error]) => new Expanded(
                        child: new Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 16.0),
                            decoration: Theme.Button.boxDecoration,
                            child: MaterialButton(
                              elevation: Theme.Button.elevation,
                              onPressed: () =>
                                  _asyncLoaderState.currentState.reloadState(),
                              child: Container(
                                padding: EdgeInsets.all(14.0),
                                child: Text(
                                  "Tekrar Dene",
                                  style: Theme.Button.textStyle,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                  renderSuccess: ({data}) => new Expanded(
                        child: Container(
                          child: (data != null && _friendRequests.isNotEmpty)
                              ? ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: _friendRequests.length,
                                  padding: const EdgeInsets.all(5.0),
                                  itemBuilder: (context, position) {
                                    return GestureDetector(
                                      onTap: () =>
                                          goToFriend(_friendRequests[position]),
                                      child: new Card(
                                          shape: Theme.Card.shape,
                                          child: new Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 8.0,
                                                horizontal: 16.0),
                                            child: new Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: new Text(
                                                    _friendRequests[position]
                                                        .username,
                                                    style:
                                                        Theme.Text.textStyle(),
                                                  ),
                                                ),
                                                IconButton(
                                                    icon: Theme.Icon.asIgnore(),
                                                    onPressed: () =>
                                                        ignoreFriend(
                                                            _friendRequests[
                                                                position])),
                                                IconButton(
                                                    icon: Theme.Icon.asAccept(),
                                                    onPressed: () =>
                                                        acceptFriend(
                                                            _friendRequests[
                                                                position])),
                                              ],
                                            ),
                                          )),
                                    );
                                  })
                              : new Container(
                                  child: Center(
                                    child: Text(
                                      "Arkadaş isteğiniz bulunmamaktadır!",
                                      style: Theme.Text.titleStyle(),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                )
              ],
            ),
          ),
          onRefresh: _handleRefresh),
    );
  }

  Future<Null> _handleRefresh() async {
    await _asyncLoaderState.currentState.reloadState();
    return null;
  }

  Future<List<User>> refreshFriendRequests() async {
    _friendRequests.clear();
    FriendRequestsResponse response =
        await Api.getFriendRequests(await SharedPreferencesHelper.getToken());
    if (response.genericResponse.success) {
      if (response.friend_requests != null)
        response.friend_requests.length > 0
            ? _friendRequests = response.friend_requests
            : _friendRequests = new List<User>();
    }

    await Future.delayed(Duration(seconds: 1));
    return _friendRequests;
  }

  Future ignoreFriend(User user) async {
    await Api.ignoreFriendRequest(await SharedPreferencesHelper.getToken(),
        new FriendRequest(user.id).toJson());

    await Api.getProfile(
      await SharedPreferencesHelper.getToken(),
    );

    _handleRefresh();
  }

  Future acceptFriend(User user) async {
    await Api.acceptFriendRequest(await SharedPreferencesHelper.getToken(),
        new FriendRequest(user.id).toJson());

    _handleRefresh();
  }

  void goToFriend(User user) {
    Navigator.of(_parentContext).push(new MaterialPageRoute(
        builder: (BuildContext context) => new OtherUserPage(user.id)));
  }
}
