import 'package:flutter/material.dart';
import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/LobbyPage.dart';
import 'package:gamerhub/utils/Constants.dart';

class LobiesListView extends StatelessWidget {
  BuildContext _parentContext;

  final List<Lobby> lobbies;

  LobiesListView(this._parentContext, this.lobbies, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: lobbies.length,
          itemBuilder: (context, position) {
            return new Card(
              shape: Theme.Card.shape,
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: new Container(
                decoration: new BoxDecoration(
                  borderRadius: Theme.Card.borderRadius,
                  image: DecorationImage(
                    image: NetworkImage(
                        Constants.API_BASE_URL + lobbies[position].game.avatar),
                    colorFilter: ColorFilter.srgbToLinearGamma(),
                    fit: BoxFit.cover,
                  ),
                  boxShadow: [BoxShadow(blurRadius: 3.0, color: Colors.black)],
                ),
                height:
                    (133 + (38 * (lobbies[position].game.rank != null ? 1 : 0)))
                        .toDouble(),
                child: GestureDetector(
                  child: new Container(
                    padding: EdgeInsets.only(
                        top: 16, right: 16, left: 16, bottom: 10),
                    decoration: new BoxDecoration(
                        borderRadius: Theme.Card.borderRadius,
                        color: const Color(0x44000000)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        new Text(
                          lobbies[position].game.name,
                          softWrap: true,
                          style: new TextStyle(
                              color: Theme.Text.cardTextColor,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                        new Container(
                            margin: EdgeInsets.symmetric(vertical: 8),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(right: 16),
                                      width: 60.0,
                                      height: 60.0,
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          image: DecorationImage(
                                              image: lobbies[position]
                                                          .owner
                                                          .avatar !=
                                                      null
                                                  ? NetworkImage(
                                                      Constants.API_BASE_URL +
                                                          lobbies[position]
                                                              .game
                                                              .avatar)
                                                  : Theme.PlaceHolder.user,
                                              fit: BoxFit.fill),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(60.0)),
                                          boxShadow: [
                                            BoxShadow(
                                                spreadRadius: 3.0,
                                                color: Theme.Colors.primarySoft)
                                          ]),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                          lobbies[position].owner.username,
                                          textAlign: TextAlign.start,
                                          softWrap: true,
                                          style: new TextStyle(
                                              color: Theme.Text.cardTextColor,
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 2),
                                          child: new Text(
                                            lobbies[position]
                                                    .lobbyCount
                                                    .toString() +
                                                " / " +
                                                lobbies[position]
                                                    .memberLimit
                                                    .toString(),
                                            style: new TextStyle(
                                              color: Theme.Text.cardTextColor,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                new Container(
                                  height: (38 *
                                          ((lobbies[position].game.rank != null
                                              ? 1
                                              : 0)))
                                      .toDouble(),
                                  margin: EdgeInsets.only(top: 8),
                                  child: ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    itemCount:
                                        (lobbies[position].game.rank != null
                                            ? 1
                                            : 0),
                                    itemBuilder: (context, rankPosition) {
                                      return new Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                            margin: EdgeInsets.only(
                                                left: 4,
                                                top: 4,
                                                bottom: 4,
                                                right: 12),
                                            width: 30.0,
                                            height: 30.0,
                                            decoration: BoxDecoration(
                                              color: Colors.grey,
                                              image: DecorationImage(
                                                  image: lobbies[position]
                                                          .game
                                                          .rank
                                                          .avatar
                                                          .isNotEmpty
                                                      ? NetworkImage(Constants
                                                              .API_BASE_URL +
                                                          lobbies[position]
                                                              .game
                                                              .rank
                                                              .avatar)
                                                      : Theme
                                                          .PlaceHolder.object,
                                                  fit: BoxFit.cover),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(75.0)),
                                              boxShadow: [
                                                BoxShadow(
                                                    spreadRadius: 3.0,
                                                    color: Theme
                                                        .Colors.primarySoft
                                                        .withOpacity(0.7))
                                              ],
                                            ),
                                          ),
                                          new Text(
                                              lobbies[position].game.rank.name,
                                              style: new TextStyle(
                                                  color:
                                                      Theme.Text.cardTextColor,
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold))
                                        ],
                                      );
                                    },
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                  onTap: () => _onTapItem(lobbies[position]),
                ),
              ),
            );
          }),
    );
  }

  void _onTapItem(Lobby lobby) {
    if (lobby != null)
      Navigator.of(_parentContext).push(new MaterialPageRoute(
        builder: (BuildContext context) => new LobbyPage(lobby),
      ));
  }
}
