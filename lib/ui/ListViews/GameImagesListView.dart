import 'package:flutter/material.dart';
import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/utils/Constants.dart';
import 'package:gamerhub/views/LobbiesView.dart';

class GameImagesListView extends StatefulWidget {
  BuildContext _parentContext;
  LobbiesView _lobisView;
  List<Game> _games = new List();

  GameImagesListView(this._parentContext, this._lobisView, this._games);

  @override
  State<StatefulWidget> createState() {
    return new GameImagesListViewState(
        this._parentContext, this._lobisView, this._games);
  }
}

class GameImagesListViewState extends State<GameImagesListView> {
  BuildContext _parentContext;
  LobbiesView _lobisView;

  final List<Game> games;
  int select = 0;

  GameImagesListViewState(this._parentContext, this._lobisView, this.games);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 105.0,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: games.length,
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
          itemBuilder: (context, position) {
            return GestureDetector(
              child: Container(
                margin: EdgeInsets.only(left: 6, right: 6),
                width: 75.0,
                height: 75.0,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  image: DecorationImage(
                      image: games[position].avatar.isNotEmpty
                          ? NetworkImage(
                              Constants.API_BASE_URL + games[position].avatar)
                          : Theme.PlaceHolder.object,
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.all(Radius.circular(75.0)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: select == position ? 3.0 : 5.0,
                        spreadRadius: select == position ? 5.0 : 0.0,
                        color: select == position
                            ? Colors.lightGreen
                            : Colors.black)
                  ],
                ),
              ),
              onTap: () => _onTapItem(games[position], position),
            );
          }),
    );
  }

  void _onTapItem(Game game, int position) {
    setState(() {
      this.select = position;
    });
    if (_lobisView != null) _lobisView.selectGame(game.sId);
  }
}
