import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gamerhub/style/theme.dart' as Theme;

class UIHelper {
  static final int SHORT_TIME = 0;
  static final int LONG_TIME = 1;

  static void showSnackBarByContext(
      final BuildContext context, final String text,
      {final duration}) {
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: Text(text.toString()),
        duration: _getDuration(duration != null ? duration : -1),
      ),
    );
  }

  static void showSnackBarByKey(
      final GlobalKey<ScaffoldState> key, final String text,
      {final duration, final textColor, final backGroundColor}) {
    key.currentState.showSnackBar(
      new SnackBar(
        backgroundColor: backGroundColor == null
            ? Theme.Button.backgroundColor
            : backGroundColor,
        content: Text(
          text.toString(),
          style: new TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              color: textColor == null ? Colors.white : textColor),
        ),
        duration: _getDuration(duration != null ? duration : -1),
      ),
    );
  }

  static Duration _getDuration(int duration) {
    if (duration == SHORT_TIME)
      return Duration(seconds: 2);
    else if (duration == LONG_TIME) return Duration(seconds: 3);
    return Duration(seconds: 2);
  }

  static WillPopCallback onBackPressed(BuildContext _context) {
    return showDialog(
          context: _context,
          builder: (context) => new AlertDialog(
                contentPadding:
                    EdgeInsets.only(top: 10, left: 24, right: 24, bottom: 0),
                title: new Text('Emin misin?'),
                content: new Text('Uygulamadan çıkmak mı istiyorsunuz?'),
                actions: <Widget>[
                  new FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text("HAYIR")),
                  new FlatButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text("EVET")),
                ],
              ),
        ) ??
        false;
  }
}

class TabIndicationPainter extends CustomPainter {
  Paint painter;
  final double dxEntry;
  final double radius;
  final double dy;

  final PageController pageController;

  TabIndicationPainter(
      {this.dxEntry = 25.0,
      this.radius = 21.0,
      this.dy = 24.0,
      this.pageController})
      : super(repaint: pageController) {
    painter = new Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final pos = pageController.position;
    double fullExtent =
        (pos.maxScrollExtent - pos.minScrollExtent + pos.viewportDimension);

    double pageOffset = pos.extentBefore / fullExtent;

    bool left2right = dxEntry < size.width / 2 - 25;
    Offset entry = new Offset(left2right ? dxEntry : size.width / 2 - 25, dy);
    Offset target = new Offset(left2right ? size.width / 2 - 25 : dxEntry, dy);

    Path path = new Path();
    path.addArc(
        new Rect.fromCircle(center: entry, radius: radius), 0.5 * pi, 1 * pi);
    path.addRect(
        new Rect.fromLTRB(entry.dx, dy - radius, target.dx, dy + radius));
    path.addArc(
        new Rect.fromCircle(center: target, radius: radius), 1.5 * pi, 1 * pi);

    canvas.translate(size.width * pageOffset, 0.0);
    canvas.drawShadow(path, Theme.Colors.background, 3.0, true);
    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(TabIndicationPainter oldDelegate) => true;
}
