class Constants {
  // API Url
  static final String API_BASE_URL = "https://player-finder-api.herokuapp.com";
  static final String SOCKET_URL = "http://player-finder-api.herokuapp.com:80";

  //Socket Type
  static final String SOCKET_MSG_TYPE_USER = "USER_MSG";
  static final String SOCKET_MSG_TYPE_LOBBY = "LOBBY_MSG";
  static final String SOCKET_MSG_TYPE_ADD_FRIEND = "ADD_FRIEND_MSG";

  // Shared Preferences keys
  static final String SP_USER_KEY = "USER";
  static final String SP_TOKEN_KEY = "TOKEN";
  static final String SP_SETTINGS_KEY = "SETTINGS";

  //Return Response
  static final String RETURN_EMAIL_SUCCESS = "SUCCESS";

  //Place Holder
  static String PLACEHOLDER_IMG_USER_URL = 'assets/images/placeholder_user.png';
  static String PLACEHOLDER_IMG_OBJECT_URL =
      'assets/images/placeholder_object.jpg';

  static String getCodeString(int code) {
    String text = "Üzgünüz Hata Oluştu :(";
    switch (code) {
      case -1:
        text = "Üzgünüz Hata Oluştu :(";
        break;
      case 1000:
        text = "Üzgünüz Hata Oluştu :(";
        break;
      case 1001:
        text = "Üzgünüz Hata Oluştu :(";
        break;
      case 2000:
        text = "Üzgünüz Hata Oluştu :(";
        break;
      case 2001:
        text = "Bu ada sahip başka bir kullanıcı mevcut :(";
        break;
      case 2002:
        text = "Bu e-postaya sahip başka bir kullanıcı mevcut :(";
        break;
      case 2003:
        text = "Kullanıcı bulunamadı :(";
        break;
      case 2004:
        text = "E-posta ve ya şifre hatalı :(";
        break;
      case 2005:
        text = "Kullanıcı profili gizli :(";
        break;
      case 2006:
        text = "Üzgünüz Hata Oluştu :(";
        break;
      case 2007:
        text = "Zaten kullanıcı ile arkadaşsınız :)";
        break;
      case 2008:
        text = "Önceden istek gönderilmiş :(";
        break;
      case 2009:
        text = "Hiçbir istek yok:(";
        break;
      case 2010:
        text = "Aynı kullanıcı :(";
        break;
      case 2011:
        text = "Mesaj önderilemedi :(";
        break;
      case 4000:
        text = "Token Key Hatalı :(";
        break;
      case 4001:
        text = "Token Key Geçersiz :(";
        break;
    }

    return text;
  }
}
