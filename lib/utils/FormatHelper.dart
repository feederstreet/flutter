import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

class DateFormatter {
  static final DateFormat dateApiFormat = DateFormat('yyyy-MM-dd');
  static final DateFormat minApiFormat = DateFormat('yyyy-MM-ddTHH:mm:ss');
  static final DateFormat minLobbyFormat = DateFormat('HH:mm');
  static final DateFormat dateFormat = DateFormat('dd/MM/yyyy');
  static final DateFormat yearFormat = DateFormat('yyyy');

  static String date(DateTime _time) {
    String val = "";
    if (_time != null) val = dateFormat.format(_time);

    print("dateByMil: String => " + val + " DateTime => " + _time.toString());
    return val;
  }

  static String dateByMil(int _time) {
    String val = "";
    if (_time != null)
      val = dateFormat.format(new DateTime.fromMillisecondsSinceEpoch(_time));
    print("dateByMil: String => " + val + " mil => " + _time.toString());
    return val;
  }

  static String dateByMilForLobby(int _time) {
    _time = _time - new DateTime.now().millisecondsSinceEpoch;
    String val = "";
    if (_time != null) {
      val =
          minLobbyFormat.format(new DateTime.fromMillisecondsSinceEpoch(_time));
    }
    print("dateByMil: String => " + val + " mil => " + _time.toString());
    return val;
  }

  static int dateToMil(String _time) {
    int val = 0;
    if (_time != null || _time.isNotEmpty)
      val = DateFormatter.dateFormat.parse(_time).millisecondsSinceEpoch;
    print("dateToMil: String => " + _time + " mil => " + val.toString());
    return val;
  }

  static int dateToMilFromApi(String _time) {
    int val = 0;

    if (_time != null) {
      if (_time.isNotEmpty)
        val = DateFormatter.dateApiFormat
            .parse(_time.split('T')[0])
            .millisecondsSinceEpoch;
    }

    print("dateToMilFromApi: String => " + _time + " mil => " + val.toString());
    return val;
  }

  static int dateToMilFromCreatedAt(String _time) {
    int val = 0;

    if (_time != null) {
      if (_time.isNotEmpty)
        val = DateFormatter.minApiFormat.parse(_time).millisecondsSinceEpoch;
    }

    print("dateToMilFromApi: String => " + _time + " mil => " + val.toString());
    return val;
  }

  static int getAsYear(int _time) {
    int val = 0;
    if (_time != null)
      val = int.parse(
          yearFormat.format(new DateTime.fromMillisecondsSinceEpoch(_time)));
    print("getAsYear: mil => " + val.toString() + " year => " + val.toString());
    return val;
  }
}
