import 'dart:async';
import 'dart:convert';

import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static Future<SharedPreferences> getSharedPreferences() async {
    return await SharedPreferences.getInstance();
  }

  static void clearSharedPreference() {
    getSharedPreferences().then((pref) {
      pref.clear();
      print("clearSharedPreference =>" + pref.toString());
    });
  }

  static putString(String value, String key) {
    print("putString: " +
        (key == null ? "null" : key) +
        " => " +
        (value == null ? "null" : value));
    getSharedPreferences().then((pref) {
      pref.setString(key, value);
    });
  }

  static putBoolean(bool value, String key) {
    print("putBoolean: " +
        (key == null ? "null" : key) +
        " => " +
        (value == null ? "null" : value.toString()));
    getSharedPreferences().then((pref) {
      pref.setBool(key, value);
    });
  }

  static putJson(Map body, String key) async {
    await getSharedPreferences().then((pref) {
      print("putJson: " +
          (key == null ? "null" : key) +
          " => " +
          (body == null ? "null" : body.toString()));
      pref.setString(key, json.encode(body));
    });
  }

  static Future<String> getString(String key) async {
    String text = await getSharedPreferences().then((pref) async {
      return await pref.get(key);
    });
    print("getString: string => " + (text == null ? "null" : text));
    return text;
  }

  static Future<bool> getBoolean(String key) async {
    bool val = await getSharedPreferences().then((pref) async {
      return await pref.get(key);
    });
    return val;
  }

  static Future<User> getUser() async {
    String val = await getString(Constants.SP_USER_KEY);
    User user = User.fromJson(json.decode(val));
    print("getUser: user => " + (user == null ? "null" : user.toString()));
    return user;
  }

  static Future<String> getToken() async {
    String val = await getString(Constants.SP_TOKEN_KEY);
    print("getToken: Token => " + (val == null ? "null" : val));
    return (val == null ? "" : val);
  }

  static Future<bool> getSettingsVal() async {
    bool val = await getBoolean(Constants.SP_SETTINGS_KEY);
    print("getSettingsVal: val => " + (val == null ? "null" : val.toString()));
    return (val == null ? false : val);
  }
}
