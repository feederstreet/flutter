import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';

class OtherUserView {
  void refreshData(User auth) {}
  void gotToMessagePage() {}
  void onResponse(User user) {}
  void onFailure(ErrorResponse error) {}
}
