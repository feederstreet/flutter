import 'dart:async';

import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/model/Lobby.dart';

class LobbiesView {
  Future<List<Game>> refreshGames() async {}

  Future<List<Lobby>> refreshLobies(String gameId) async {}

  void selectGame(String gameId) async {}

  void goToFilter() async {}

  void addLobi() async {}
}
