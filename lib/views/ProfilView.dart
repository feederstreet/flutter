import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';

class ProfilView {
  void refreshData(User auth) {}
  void goToFriends() {}
  void goToSettings() {}
  void onResponse(User user) {}
  void onFailure(ErrorResponse error) {}
}
