import 'package:gamerhub/network/response/ErrorResponse.dart';

class SignInView {
  void onSignInResponse() {}

  void onSignInFailure(ErrorResponse error) {}

  void onSocialSignInFailure(String errorText) {}

  void _signIn() {}
}
