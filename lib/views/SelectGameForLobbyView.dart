import 'dart:async';

import 'package:gamerhub/model/Game.dart';

class SelectGameForLobbyView {
  Future<List<Game>> refreshGames() async {}

  void selectGame(Game selectGame) async {}
}
