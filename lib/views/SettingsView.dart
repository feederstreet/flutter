import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/response/ErrorResponse.dart';

class SettingsView {
  void refreshImage(String photoUrl) {}
  void refreshData(User auth) {}
  void onResponse(User user) {}
  void onFailure(ErrorResponse error) {}
}
