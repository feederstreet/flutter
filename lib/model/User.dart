import 'package:gamerhub/utils/FormatHelper.dart';

class User {
  String id;
  String fullname;
  String username;
  bool gender;
  int birthDate;
  String email;
  String avatar;
  int lastSeen;
  int karmaPoint;
  bool profileVisibility;
  String password;
  int googleId;
  int facebookId;
  String lobbyId;
  String facebookUrl;
  String discordUrl;

  User(
      {this.id,
      this.fullname,
      this.username,
      this.gender,
      this.birthDate,
      this.email,
      this.avatar,
      this.lastSeen,
      this.karmaPoint,
      this.password,
      this.profileVisibility,
      this.googleId,
      this.facebookId,
      this.lobbyId,
      this.facebookUrl,
      this.discordUrl});

  User.fromJson(Map<String, dynamic> json) {
    if (json.containsKey("_id"))
      id = json['_id'];
    else if (json.containsKey("id")) id = json['id'];
    if (json['lobby_id'] != null) lobbyId = json['lobby_id'];
    password = json['password'];
    fullname = json['fullname'];
    username = json['username'];
    gender = json['gender'];
    if (json['birth_date'] != null)
      birthDate = DateFormatter.dateToMilFromApi(json['birth_date']);
    email = json['email'];
    avatar = json['avatar'];
    lastSeen = json['last_seen'];
    karmaPoint = json['karma_point'];
    profileVisibility = json['profile_visibility'];
    googleId = json['google_id'];
    facebookId = json['facebook_id'];
    if (json.containsKey('contact')) {
      Map<String, dynamic> contacts = json['contact'];
      if (contacts.containsKey('facebook_url'))
        facebookUrl = contacts['facebook_url'];
      if (contacts.containsKey('discord_url'))
        discordUrl = contacts['discord_url'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['id'] = this.id;
    data['fullname'] = this.fullname;
    data['username'] = this.username;
    data['lobby_id'] = this.lobbyId;
    data['password'] = this.password;
    data['gender'] = this.gender;
    data['birth_date'] = this.birthDate;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    data['last_seen'] = this.lastSeen;
    data['karma_point'] = this.karmaPoint;
    data['profile_visibility'] = this.profileVisibility;
    data['google_id'] = this.googleId;
    data['facebook_id'] = this.facebookId;
    final Map<String, dynamic> contacts = new Map<String, dynamic>();
    contacts['facebook_url'] = this.facebookUrl;
    contacts['discord_url'] = this.discordUrl;
    data['contact'] = contacts;
    return data;
  }

  @override
  String toString() {
    return 'User{id: $id, fullname: $fullname, username: $username, gender: $gender, birthDate: $birthDate, email: $email, avatar: $avatar, lastSeen: $lastSeen, karmaPoint: $karmaPoint, profileVisibility: $profileVisibility, password: $password, googleId: $googleId, facebookId: $facebookId, lobbyId: $lobbyId, facebookUrl: $facebookUrl, discordUrl: $discordUrl}';
  }
}
