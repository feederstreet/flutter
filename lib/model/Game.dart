import 'package:gamerhub/model/Rank.dart';

class Game {
  String _sId;
  String _name;
  String _shortName;
  String _avatar;
  List<Rank> _ranks;
  Rank _rank;

  Game(
      {String sId,
      String name,
      String shortName,
      String avatar,
      List<Rank> ranks,
      Rank rank}) {
    this._sId = sId;
    this._name = name;
    this._shortName = shortName;
    this._avatar = avatar;
    this._ranks = ranks;
    this._rank = rank;
  }

  String get sId => _sId;

  set sId(String sId) => _sId = sId;

  String get name => _name;

  set name(String name) => _name = name;

  String get shortName => _shortName;

  set shortName(String shortName) => _shortName = shortName;

  String get avatar => _avatar;

  set avatar(String avatar) => _avatar = avatar;

  List<Rank> get ranks => _ranks;

  set ranks(List<Rank> ranks) => _ranks = ranks;

  Rank get rank => _rank;

  set rank(Rank value) {
    _rank = value;
  }

  Game.fromJson(Map<String, dynamic> json) {
    _sId = json['_id'];
    _name = json['name'];
    _shortName = json['short_name'];
    _avatar = json['avatar'];

    if (json.containsKey('ranks')) {
      _ranks = new List<Rank>();
      json['ranks'].forEach((v) {
        _ranks.add(new Rank.fromJson(v));
      });
    }

    if (json.containsKey('rank')) {
      _rank = Rank.fromJson(json['rank']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this._sId;
    data['name'] = this._name;
    data['short_name'] = this._shortName;
    data['avatar'] = this._avatar;
    if (this._ranks != null) {
      data['ranks'] = this._ranks.map((v) => v.toJson()).toList();
    }
    if (this._rank != null) {
      data['rank'] = this.rank.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return 'Game{_sId: $_sId, _name: $_name, _shortName: $_shortName, _avatar: $_avatar, _ranks: $_ranks}';
  }
}
