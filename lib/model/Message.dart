class Message {
  String sId;
  String content;
  String username;

  Message({this.sId, this.content, this.username});

  Message.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    content = json['content'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['content'] = this.content;
    data['username'] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'Message{sId: $sId, content: $content, username: $username}';
  }
}
