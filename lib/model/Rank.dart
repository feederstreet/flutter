class Rank {
  String _name;
  String _avatar;

  Rank({String name, String avatar}) {
    this._name = name;
    this._avatar = avatar;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get avatar => _avatar;
  set avatar(String avatar) => _avatar = avatar;

  Rank.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['avatar'] = this._avatar;
    return data;
  }

  @override
  String toString() {
    return 'Rank{_name: $_name, _avatar: $_avatar}';
  }
}
