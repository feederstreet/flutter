class Conversation {
  String _userId;
  String _message;
  String _receivedAt;

  Conversation({String userId, String message, String receivedAt}) {
    this._userId = userId;
    this._message = message;
    this._receivedAt = receivedAt;
  }

  String get userId => _userId;
  set userId(String _userId) => userId = _userId;
  String get message => _message;
  set message(String message) => _message = message;
  String get receivedAt => _receivedAt;
  set receivedAt(String receivedAt) => _receivedAt = receivedAt;

  Conversation.fromJson(Map<String, dynamic> json) {
    _userId = json.containsKey('from') ? ((json['from'])['user'])['id'] : "";
    _message = json['message'];
    _receivedAt = json['received_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._userId != null) {
      Map<String, dynamic> id = new Map<String, dynamic>();
      id['id'] = this._userId;
      Map<String, dynamic> user = new Map<String, dynamic>();
      user['user'] = id;
      data['from'] = user;
    }
    data['message'] = this._message;
    data['received_at'] = this._receivedAt;
    return data;
  }

  @override
  String toString() {
    return 'Conversation{_userId: $_userId, _message: $_message, _receivedAt: $_receivedAt}';
  }
}
