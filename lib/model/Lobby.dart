import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/utils/FormatHelper.dart';

class Lobby {
  String _id;
  int _type;
  User _owner;
  List<User> _members;
  int _memberLimit;
  Game _game;
  int _createdAt;

  Lobby(
      {String id,
      int type,
      User owner,
      List<User> members,
      int memberLimit,
      Game game,
      int createdAt}) {
    this._id = id;
    this._type = type;
    this._owner = owner;
    this._members = members;
    this._memberLimit = memberLimit;
    this._game = game;
  }

  int get type => _type;

  set type(int type) => _type = type;

  User get owner => _owner;

  set owner(User owner) => _owner = owner;

  List<User> get members => _members;

  set members(List<User> members) => _members = members;

  int get memberLimit => _memberLimit;

  set memberLimit(int memberLimit) => _memberLimit = memberLimit;

  Game get game => _game;

  set game(Game game) => _game = game;

  int get createdAt => _createdAt;

  set createdAt(int value) => _createdAt = value;

  String get id => _id;

  set id(String value) => _id = value;

  int get lobbyCount => members == null
      ? (owner != null ? 1 : 0)
      : members.length + (owner != null ? 1 : 0);

  Lobby.fromJson(Map<String, dynamic> jsonLobby) {
    id = jsonLobby['_id'];
    _type = jsonLobby['type'];
    _owner = jsonLobby['owner'] != null
        ? new User.fromJson(jsonLobby['owner'])
        : null;
    if (jsonLobby['members'] != null) {
      _members = new List<User>();
      jsonLobby['members'].forEach((v) {
        print(v['user'].toString());
        _members.add(new User.fromJson(v['user']));
      });
    }
    _memberLimit = jsonLobby['member_limit'];
    _game =
        jsonLobby['game'] != null ? new Game.fromJson(jsonLobby['game']) : null;
    _createdAt = DateFormatter.dateToMilFromCreatedAt(jsonLobby['created_at']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['type'] = this._type;
    if (this._owner != null) {
      data['owner'] = this._owner.toJson();
    }
    if (this._members != null) {
      data['members'] = this._members.map((v) => v.toJson()).toList();
    }
    data['member_limit'] = this._memberLimit;
    if (this._game != null) {
      data['game'] = this._game.toJson();
    }

    return data;
  }

  @override
  String toString() {
    return 'Lobby{_type: $_type, _owner: $_owner, _members: $_members, _memberLimit: $_memberLimit, _game: $_game}';
  }
}
