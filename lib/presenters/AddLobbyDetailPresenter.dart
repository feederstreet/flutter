import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/views/AddLobbyDetailView.dart';

class Presenter {}

class AddLobbyDetailPresenter implements Presenter {
  AddLobbyDetailView _view;

  List<Game> _games;

  List<Game> get games => _games == null ? new List() : _games;

  set games(List<Game> value) {
    _games = value;
  }

  AddLobbyDetailPresenter(this._view);
}
