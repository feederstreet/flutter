import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/lobby/LobbyRequest.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/network/response/LobbyResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/LobbyView.dart';

class Presenter {
  void joinLobby(Lobby _lobby) async {}

  void exitLobby(Lobby _lobby) async {}

  Future<Lobby> refreshLobby(Lobby _lobby) async {}
}

class LobbyPresenter implements Presenter {
  LobbyView _view;

  LobbyPresenter(this._view);

  @override
  void exitLobby(Lobby _lobby) async {
    GenericResponse response =
        await Api.exitLobby(await SharedPreferencesHelper.getToken());

    response.success ? _view.exit() : _view.onFailure(response.error);
  }

  @override
  void joinLobby(Lobby _lobby) async {
    LobbyRequest request = new LobbyRequest();
    request.lobbyId = _lobby.id;

    GenericResponse response = await Api.joinLobby(
        await SharedPreferencesHelper.getToken(), request.toJson());

    response.success ? _view.join() : _view.onFailure(response.error);
  }

  @override
  Future<Lobby> refreshLobby(Lobby _lobby) async {
    LobbyResponse response = (await Api.getLobby(
        (await SharedPreferencesHelper.getToken()),
        new LobbyRequest(lobbyId: _lobby.id).toJson()));

    if (response.genericResponse.success) return response.lobby;
    return _lobby;
  }
}
