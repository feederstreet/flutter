import 'dart:convert';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/auth/SignInRequest.dart';
import 'package:gamerhub/network/request/auth/SignWithFacebookRequest.dart';
import 'package:gamerhub/network/request/auth/SignWithGoogleRequest.dart';
import 'package:gamerhub/network/response/SignResponse.dart';
import 'package:gamerhub/network/response/SocialResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/SignInView.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

class Presenter {
  void onSignIn(String eMail, String password) {}

  void onSignInFaceBook() {}

  void onSignInGoogle() async {}
}

class SignInPresenter implements Presenter {
  SignInView _signInView;

  /* Google */
  GoogleSignIn _googleSignIn;

  /* Facebook */
  FacebookLogin _facebookLogin;

  SignInPresenter(this._signInView) {
    //Facebook Sign In
    _facebookLogin = FacebookLogin();

    // Google Sign In
    _googleSignIn = GoogleSignIn(
      scopes: <String>[
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/contacts.readonly"
      ],
    );

    _googleSignIn.onCurrentUserChanged
        .listen((GoogleSignInAccount account) async {
      if (account != null) {
        SignWithGoogleRequest googleRequest = new SignWithGoogleRequest();
        googleRequest.fullname = account.displayName;
        googleRequest.email = account.email;
        googleRequest.google_id = account.id;

        SignResponse response =
            await Api.signInWithGoogle(googleRequest.toJson());

        if (response.genericResponse.success) {
          Api.getProfile(await SharedPreferencesHelper.getToken())
              .then((response) => {_signInView.onSignInResponse()});
        } else {
          _signInView.onSignInFailure(response.genericResponse.error);
        }
      } else {
        _signInView.onSocialSignInFailure("Google ile giriş yapılamadı!");
      }
    });
  }

  @override
  void onSignIn(String eMail, String password) async {
    SignResponse response = await Api.signIn(
        new SignInRequest(usernameOrEmail: "deneme", password: "3569466")
            .toJson());

    if (response.genericResponse.success) {
      await Api.getProfile(await SharedPreferencesHelper.getToken());
      _signInView.onSignInResponse();
    } else {
      _signInView.onSignInFailure(response.genericResponse.error);
    }
  }

  @override
  void onSignInFaceBook() async {
    try {
      _facebookLogin.logOut();
    } catch (e) {} finally {
      await _facebookLogin.logInWithReadPermissions(
          ['email', 'public_profile']).then((result) async {
        switch (result.status) {
          case FacebookLoginStatus.error:
            print("DENEME -FACEBOOK => error => " + result.errorMessage);
            _signInView.onSocialSignInFailure("Facebook ile giriş yapılamadı!");
            break;
          case FacebookLoginStatus.cancelledByUser:
            print("DENEME -FACEBOOK => cancelledByUser => " +
                result.errorMessage);
            _signInView.onSocialSignInFailure("Facebook ile giriş yapılamadı!");
            break;
          case FacebookLoginStatus.loggedIn:
            var graphResponse = await http.get(
                'https://graph.facebook.com/v2.12/me?fields=name,email&access_token=${result.accessToken.token}');

            print("FACEBOOK:" + graphResponse.body);
            SocialResponse facebookResponse =
                SocialResponse.fromJson(json.decode(graphResponse.body));

            if (facebookResponse != null) {
              SignWithFacebookRequest facebookRequest =
                  new SignWithFacebookRequest();
              facebookRequest.email = facebookResponse.email;
              facebookRequest.fullname = facebookResponse.name;
              facebookRequest.facebook_id = facebookResponse.id;

              SignResponse response =
                  await Api.signInWithFacebook(facebookRequest.toJson());

              if (response.genericResponse.success) {
                await Api.getProfile(await SharedPreferencesHelper.getToken());
                _signInView.onSignInResponse();
              } else {
                _signInView.onSignInFailure(response.genericResponse.error);
              }
            } else {
              _signInView
                  .onSocialSignInFailure("Facebook ile giriş yapılamadı!");
            }
            break;
        }
      }).catchError((error) {
        _signInView.onSocialSignInFailure("Facebook ile giriş yapılamadı!");
      });
    }
  }

  @override
  void onSignInGoogle() async {
    try {
      _googleSignIn.disconnect();
    } catch (e) {} finally {
      await _googleSignIn.signIn().catchError((error) {
        _signInView.onSocialSignInFailure("Google ile giriş yapılamadı!");
      });
    }
  }
}
