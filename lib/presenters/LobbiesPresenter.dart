import 'dart:async';

import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/lobby/LobbiesRequest.dart';
import 'package:gamerhub/network/response/LobbiesResponse.dart';
import 'package:gamerhub/network/response/ProfileResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/LobbiesView.dart';

class Presenter {
  Future<List<Game>> getGames() async {}

  Future<List<Lobby>> getLobbies(String gameId) async {}

  Future<bool> hasMyLobby() async {}
}

class LobbiesPresenter implements Presenter {
  LobbiesView _view;

  List<Lobby> _lobbies;
  List<Game> _games;

  List<Lobby> get lobis => _lobbies == null ? new List() : _lobbies;

  set lobbies(List<Lobby> value) {
    _lobbies = value;
  }

  List<Game> get games => _games == null ? new List() : _games;

  set games(List<Game> value) {
    _games = value;
  }

  LobbiesPresenter(this._view);

  @override
  Future<List<Game>> getGames() async {
    final response =
        await Api.getGames(await SharedPreferencesHelper.getToken());
    if (response.genericResponse.success)
      return response.games;
    else
      return new List();
  }

  @override
  Future<List<Lobby>> getLobbies(String gameId) async {
    LobbiesResponse response = await Api.getLobbies(
        await SharedPreferencesHelper.getToken(),
        new LobbiesRequest(gameId: gameId).toJson());

    if (response.genericResponse.success)
      return response.lobbies;
    else
      return new List();
  }

  @override
  Future<bool> hasMyLobby() async {
    ProfileResponse response =
        (await Api.getProfile(await SharedPreferencesHelper.getToken()));

    if (response.user.lobbyId != null) {
      if (response.user.lobbyId.isNotEmpty) return true;
    }

    return false;
  }
}
