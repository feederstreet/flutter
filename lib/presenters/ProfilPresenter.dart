import 'dart:async';

import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/lobby/LobbyRequest.dart';
import 'package:gamerhub/network/response/LobbyResponse.dart';
import 'package:gamerhub/network/response/ProfileResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/ProfilView.dart';

class Presenter {
  void changeProfilPhoto() {}

  Future<ProfileResponse> initView() {}

  Future<int> getRequestCount() {}

  Future<Lobby> getLobby(String lobby_id) {}
}

class ProfilPresenter implements Presenter {
  ProfilView _profilView;

  ProfilPresenter(this._profilView);

  @override
  void changeProfilPhoto() {
    // TODO: iProfil Photo Değiştirme Eklenecek
  }

  @override
  Future<ProfileResponse> initView() async {
    ProfileResponse response =
        await Api.getProfile(await SharedPreferencesHelper.getToken());

    if (response.genericResponse.success) {
      _profilView.onResponse(await SharedPreferencesHelper.getUser());
    } else {
      _profilView.onFailure(response.genericResponse.error);
    }
    return response;
  }

  Future<Lobby> getLobby(String lobby_id) async {
    LobbyResponse response = (await Api.getLobby(
        (await SharedPreferencesHelper.getToken()),
        new LobbyRequest(lobbyId: lobby_id).toJson()));

    if (response.genericResponse.success) return response.lobby;

    return null;
  }

  @override
  Future<int> getRequestCount() async {
    int count =
        (await Api.getFriendRequests(await SharedPreferencesHelper.getToken()))
            .friend_requests
            .length;
    return count;
  }
}
