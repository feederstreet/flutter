import 'dart:async';

import 'package:gamerhub/model/User.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/ChangeSettingsRequest.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/SettingsView.dart';

class Presenter {
  void changeProfilPhoto() {}

  void initView() {}

  void changeSettings(ChangeSettingsRequest request) async {}
}

class SettingsPresenter implements Presenter {
  SettingsView _settingsView;

  SettingsPresenter(this._settingsView);

  @override
  void changeProfilPhoto() {
    _settingsView.refreshImage("https://picsum.photos/250?image=9");
  }

  @override
  Future<User> initView() async {
    try {
      await _settingsView.refreshData(await SharedPreferencesHelper.getUser());
    } catch (e) {}
    return await SharedPreferencesHelper.getUser();
  }

  @override
  changeSettings(ChangeSettingsRequest request) async {
    if (request != null) {
      GenericResponse response = await Api.changeSettings(
          await SharedPreferencesHelper.getToken(), request.toJson());

      if (response.success) {
        return await _settingsView.onResponse(
            (await Api.getProfile(await SharedPreferencesHelper.getToken()))
                .user);
      } else {
        return _settingsView.onFailure(response.error);
      }
    }
  }
}
