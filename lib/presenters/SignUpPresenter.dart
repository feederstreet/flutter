import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/auth/SignUpRequest.dart';
import 'package:gamerhub/network/response/SignResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/SignUpView.dart';

class Presenter {
  void signUp(String name, String email, String password) {}
}

class SignUpPresenter implements Presenter {
  SignUpView _signUpView;

  SignUpPresenter(this._signUpView);

  @override
  void signUp(String name, String email, String password) async {
    SignUpRequest request = new SignUpRequest();
    request.username = name;
    request.email = email;
    request.password = password;

    SignResponse response = await Api.signUp(request.toJson());

    if (response.genericResponse.success) {
      await Api.getProfile(await SharedPreferencesHelper.getToken());
      _signUpView.onResponse();
    } else {
      _signUpView.onFailure(response.genericResponse.error);
    }
  }
}
