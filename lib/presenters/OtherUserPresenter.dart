import 'package:gamerhub/model/Lobby.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/lobby/LobbyRequest.dart';
import 'package:gamerhub/network/request/user/FriendRequest.dart';
import 'package:gamerhub/network/response/GenericResponse.dart';
import 'package:gamerhub/network/response/LobbyResponse.dart';
import 'package:gamerhub/network/response/ProfileResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/OtherUserView.dart';

class Presenter {
  Future<ProfileResponse> initView(String targetId) {}

  Future<bool> isFriend(String targetId) {}

  Future<bool> hasFriendRequest(String targetId) {}

  void addFriend(String targetId) {}

  void cancelRequest(String targetId) {}

  void removeFriend(String targetId) {}

  Future<Lobby> getLobby(String lobby_id) {}
}

class OtherUserPresenter implements Presenter {
  OtherUserView _otherUserView;

  OtherUserPresenter(this._otherUserView);

  @override
  Future<ProfileResponse> initView(String targetId) async {
    ProfileResponse response = await Api.getFriendProfile(
        await SharedPreferencesHelper.getToken(), targetId);

    if (response.genericResponse.success) {
      _otherUserView.onResponse(response.user);
    } else {
      _otherUserView.onFailure(response.genericResponse.error);
    }
    return response;
  }

  @override
  Future<bool> isFriend(String targetId) async {
    bool isFriend = (await Api.isFriend(
            await SharedPreferencesHelper.getToken(),
            new FriendRequest(targetId).toJson()))
        .success;
    return isFriend;
  }

  @override
  void addFriend(String targetId) async {
    GenericResponse response = await Api.addFriend(
        await SharedPreferencesHelper.getToken(),
        new FriendRequest(targetId).toJson());

    if (response.success) {
      initView(targetId);
    } else {
      _otherUserView.onFailure(response.error);
    }
  }

  @override
  void removeFriend(String targetId) async {
    GenericResponse response = await Api.removeFriend(
        await SharedPreferencesHelper.getToken(),
        new FriendRequest(targetId).toJson());

    if (response.success) {
      initView(targetId);
    } else {
      _otherUserView.onFailure(response.error);
    }
  }

  @override
  Future<bool> hasFriendRequest(String targetId) async {
    GenericResponse response = await Api.hasFriendRequest(
        await SharedPreferencesHelper.getToken(),
        new FriendRequest(targetId).toJson());
    return response.success;
  }

  @override
  Future cancelRequest(String targetId) async {
    GenericResponse response = await Api.cancelFriendRequest(
        await SharedPreferencesHelper.getToken(),
        new FriendRequest(targetId).toJson());

    if (response.success) {
      initView(targetId);
    } else {
      _otherUserView.onFailure(response.error);
    }
  }

  Future<Lobby> getLobby(String lobby_id) async {
    LobbyResponse response = (await Api.getLobby(
        (await SharedPreferencesHelper.getToken()),
        new LobbyRequest(lobbyId: lobby_id).toJson()));

    if (response.genericResponse.success) return response.lobby;

    return null;
  }
}
