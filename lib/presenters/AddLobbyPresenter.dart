import 'dart:async';

import 'package:gamerhub/model/Game.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/SelectGameForLobbyView.dart';

class Presenter {
  Future<List<Game>> getGames() async {}
}

class AddLobbyPresenter implements Presenter {
  SelectGameForLobbyView _view;

  List<Game> _games;

  List<Game> get games => _games == null ? new List() : _games;

  set games(List<Game> value) {
    _games = value;
  }

  AddLobbyPresenter(this._view);

  @override
  Future<List<Game>> getGames() async {
    final response =
        await Api.getGames(await SharedPreferencesHelper.getToken());

    if (response.genericResponse.success) {
      print(response.toString());
      return response.games;
    } else {
      print("null");
      return new List();
    }
  }
}
