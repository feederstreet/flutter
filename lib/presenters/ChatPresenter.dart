import 'package:gamerhub/model/Conversation.dart';
import 'package:gamerhub/network/Api.dart';
import 'package:gamerhub/network/request/user/ConversationRequest.dart';
import 'package:gamerhub/network/response/ConversationResponse.dart';
import 'package:gamerhub/utils/SharedPreferencesHelper.dart';
import 'package:gamerhub/views/ChatView.dart';

class Presenter {
  void sendMessage(String to, String message) {}

  Future<List<Conversation>> getConversation(String targetId) async {}
}

class ChatPresenter implements Presenter {
  ChatView _chatView;

  ChatPresenter(this._chatView);

  @override
  void sendMessage(String to, String message) {
    // TODO: implement sendMessage
  }

  @override
  Future<List<Conversation>> getConversation(String targetId) async {
    ConversationRequest request = new ConversationRequest(targetId);
    ConversationResponse response = await Api.getConversation(
        await SharedPreferencesHelper.getToken(), request.toJson());
    if (response.inbox != null) {
      return response.inbox;
    }

    return new List();
  }
}
