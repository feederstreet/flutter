import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart' as Material;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gamerhub/utils/Constants.dart';

class Colors {
  static final Color primarySoft = const Color(0xFFef9a9a);
  static final Color primary = const Color(0xFFf44336);
  static final Color primaryDark = const Color(0xFFd32f2f);
  static final Color accent = const Color(0xFFFF3D00);
  static final Color colorButton = Material.Colors.deepOrangeAccent

      /*Material.Colors.lightBlue*/;

  static Color colorPrimaryGradient() {
    return (Material.Color.fromARGB(255, new Random().nextInt(255),
        new Random().nextInt(255), new Random().nextInt(255)));
  }

  static Color colorSecondaryGradient() {
    return (Material.Color.fromARGB(255, new Random().nextInt(255),
        new Random().nextInt(255), new Random().nextInt(255)));
  }

  static final Color hint = const Color(0x55000000);

  static final Color colorFacebook = const Color(0xFF3b5999);
  static final Color colorDiscord = const Color(0xFF738ADB);
  static final Color colorGoogle = const Color(0xFFDB4437);

  static final background = const Color(0xFFF5F5F5);
  static final backgroundCard = const Color(0xFFFFFFFF);

  static final Color hintColor = Material.Colors.grey;
}

class Button {
  static final elevation = 2.0;
  static final textColor = Material.Colors.white;
  static final backgroundColor = Colors.colorButton;
  static final borderRadius = Material.BorderRadius.circular(5.0);
  static final borderRoundedRadius = Material.BorderRadius.circular(64.0);

  static final textStyle = TextStyle(
    color: textColor,
    fontSize: 16.0,
  );

  static final textCountStyle = TextStyle(
    color: textColor,
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  );

  static Widget textCount(int count) {
    return (count != null && count > 0)
        ? Padding(
            padding: EdgeInsets.only(left: 5),
            child: new ClipOval(
              child: Container(
                color: Material.Colors.white,
                width: 22,
                height: 22,
                child: Center(
                    child: new Material.Text(
                  count.toString(),
                  style: TextStyle(
                    color: backgroundColor,
                  ),
                )),
              ),
            ),
          )
        : new Container();
  }

  static final Material.BoxDecoration boxDecoration = Material.BoxDecoration(
    borderRadius: borderRadius,
    color: backgroundColor,
  );

  static final Material.BoxDecoration roundedDecoration =
      Material.BoxDecoration(
    borderRadius: borderRoundedRadius,
    color: backgroundColor,
  );
}

class Card {
  static final borderRadius = Material.BorderRadius.circular(8.0);
  static final backgroundColor = Colors.backgroundCard;
  static final elevation = 2.0;
  static final shape = RoundedRectangleBorder(
    borderRadius: borderRadius,
  );
}

class Icon {
  static final borderRadius = Material.BorderRadius.circular(64.0);
  static final backgroundColor = Material.Colors.transparent;
  static final iconColor = const Color(0xFF212121);
  static final iconAppBarColor = Material.Colors.white;
  static final size = 24.0;

  static Material.Icon instance(IconData icon) {
    return new Material.Icon(
      icon,
      color: iconColor,
      size: size,
    );
  }

  static Container asSocial(IconData icon, Color color) {
    return Container(
      padding: const EdgeInsets.all(15.0),
      decoration: Material.BoxDecoration(
        color: color,
        borderRadius: borderRadius,
      ),
      child: new Material.Icon(
        icon,
        color: Material.Colors.white,
        size: size,
      ),
    );
  }

  static Material.Icon asDelete() {
    return new Material.Icon(
      Icons.delete_forever,
      color: Material.Colors.red,
      size: size,
    );
  }

  static Material.Icon asIgnore() {
    return new Material.Icon(
      Icons.close,
      color: Material.Colors.red,
      size: size,
    );
  }

  static Material.Icon asAccept() {
    return new Material.Icon(
      Icons.check,
      color: Material.Colors.green,
      size: size,
    );
  }
}

class Line {
  static final height = 1.0;
  static final color = Material.Colors.grey.shade400;

  static final left = (double width) => Container(
        decoration: BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Colors.background,
                color,
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        width: width,
        height: height,
      );

  static final both = (double width) => Container(
        width: width,
        height: height,
        color: color,
      );

  static final right = (double width) => Container(
        decoration: BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                color,
                Colors.background,
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        width: width,
        height: height,
      );
}

class Text {
  static final backgroundColor = Material.Colors.transparent;
  static final Color hintColor = const Color(0xFF757575);
  static final Color textColor = const Color(0xFF212121);
  static final Color cardTextColor = const Color(0xFFFFFFFF);
  static final fontFamily = "WorkSansMedium";
  static final border = InputBorder.none;
  static final padding = EdgeInsets.only(bottom: 10.0);

  static final titleStyle = ({double size}) => TextStyle(
      fontSize: size != null ? size : 18.0,
      color: Colors.primary,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold);

  static final headerStyle = ({double size}) => TextStyle(
      fontSize: size != null ? size : 18.0,
      color: textColor,
      fontFamily: fontFamily,
      fontWeight: FontWeight.bold);

  static final textStyle = ({double size}) => TextStyle(
      fontSize: size != null ? size : 14.0,
      color: textColor,
      fontFamily: fontFamily);

  static final labelStyle = ({double size}) => TextStyle(
      color: hintColor,
      fontSize: size != null ? size : 14.0,
      fontFamily: fontFamily);

  static final errorStyle = ({double size}) => TextStyle(
      color: Material.Colors.red,
      fontSize: size != null ? size : 14.0,
      fontFamily: fontFamily);
}

class TextField {
  static final backgroundColor = Colors.background;
  static final Color hintColor = Material.Colors.grey;
  static final Color textColor = Material.Colors.black;
  static final border = InputBorder.none;
  static final padding = EdgeInsets.only(bottom: 10.0);
  static final TextStyle textStyle = Text.textStyle(size: 14.0);

  static final TextStyle labelStyle = Text.labelStyle(size: 14.0);

  static final TextStyle errorStyle = Text.errorStyle(size: 14.0);

  static final passwordIcon = Material.Icon(
    FontAwesomeIcons.eye,
    size: 15.0,
    color: Icon.iconColor,
  );
}

class AppBar {
  static final theme = AppBarTheme(
    color: Colors.background,
    elevation: 2,
    textTheme: TextTheme(
      title: Text.titleStyle(),
    ),
    iconTheme: IconThemeData(
      size: Icon.size,
      color: Colors.primary,
    ),
  );
}

class Background {
  static final borderRadius = Material.BorderRadius.circular(8.0);
  static final backgroundColor = Colors.background;
  static final shape = RoundedRectangleBorder(
    borderRadius: borderRadius,
  );

  static final decoration = Material.BoxDecoration(
    color: backgroundColor,
  );
}

class PlaceHolder {
  static var user = AssetImage(Constants.PLACEHOLDER_IMG_USER_URL);
  static var object = AssetImage(Constants.PLACEHOLDER_IMG_OBJECT_URL);
}
