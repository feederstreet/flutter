import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gamerhub/style/theme.dart' as Theme;
import 'package:gamerhub/ui/Pages/SplashPage.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  runApp(
    MaterialApp(
      theme: new ThemeData(
          bottomAppBarTheme: BottomAppBarTheme(
            elevation: 2,
          ),
          appBarTheme: Theme.AppBar.theme,
          dividerColor: Theme.Colors.primaryDark,
          indicatorColor: Theme.Colors.primarySoft,
          iconTheme: IconThemeData(
            color: Theme.Icon.iconColor,
          ),
          primaryColor: Theme.Colors.primary,
          primaryColorDark: Theme.Colors.primaryDark,
          accentColor: Theme.Colors.accent,
          primaryIconTheme: IconThemeData(color: Theme.Icon.iconAppBarColor),
          accentIconTheme: IconThemeData(color: Colors.white)),
      debugShowCheckedModeBanner: false,
      home: new SplashPage(),
    ),
  );
}
